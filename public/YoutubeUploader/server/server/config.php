<?php

// OAUTH Configuration
$oauthClientID = '96984430772-gn1ir5u16jrlfvfspgcoo445gh7km4n5.apps.googleusercontent.com';
$oauthClientSecret = 'EmTRqJcJKk9mSPpGtoSXRtJv';
$baseURL = 'http://localhost/';
$redirectURL = 'http://localhost/upload_video_to_youtube_php/config.php';
$scope = array('https://www.googleapis.com/auth/youtube.upload', 'https://www.googleapis.com/auth/youtube', 'https://www.googleapis.com/auth/youtubepartner');

define('OAUTH_CLIENT_ID',$oauthClientID);
define('OAUTH_CLIENT_SECRET',$oauthClientSecret);
define('REDIRECT_URL',$redirectURL);
define('BASE_URL',$baseURL);

// Include google client libraries
require_once '../../google-api-php/autoload.php';
require_once '../../google-api-php/Client.php';
require_once '../../google-api-php/Service/YouTube.php';

session_start();

$client = new Google_Client();
$client->setClientId(OAUTH_CLIENT_ID);
$client->setClientSecret(OAUTH_CLIENT_SECRET);
$client->setRedirectUri(REDIRECT_URL);
$client->setScopes($scope);
$client->setAccessType('offline');

// Define an object that will be used to make all API requests.
$youtube = new Google_Service_YouTube($client);



