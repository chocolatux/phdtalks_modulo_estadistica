<?
/**
 * Creacion 06 de Agosto del 2009
 * @author David Heredia
 */

class Email
{
  var $FROM;
  var $TO;
  var $SUBJECT;
  var $HEADER;
  var $CONTENT_HTML;
  var $ATTACHMENTS;

  /**
   * El constructor inicializa el objeto
   *
   * @return void
   *
   * Creacion 06 de Agosto del 2009
   * @author David Heredia
   */
  function Email()
  {
    $this->TO = array();
    $this->ATTACHMENTS = array();
  }

  /**
   * Asigna el remitente
   *
   * @param string $FROM email del remitente
   * @return void
   *
   * Creacion 06 de Agosto del 2009
   * @author David Heredia
   */
  function set_sender($FROM)
  {
    $this->FROM = $FROM;
  }

  /**
   * Agrega un destinatario
   *
   * @param string $RECIPIENT email del destinatario
   * @return void
   *
   * Creacion 06 de Agosto del 2009
   * @author David Heredia
   */
  function add_recipient($RECIPIENT)
  {
    $this->TO[] = $RECIPIENT;
  }

  /**
   * Agrega un encabezado
   *
   * @param string $HEADER encabezado a asignar
   * @param string $VALUE valor del encabezado
   * @return void
   *
   * Creacion 06 de Agosto del 2009
   * @author David Heredia
   */
  function add_header($HEADER, $VALUE)
  {
    $this->HEADER .= "$HEADER: $VALUE\n";
  }

  /**
   * Asigna el titulo del correo
   *
   * @param string $SUBJECT titulo del correo
   * @return void
   *
   * Creacion 06 de Agosto del 2009
   * @author David Heredia
   */
  function set_subject($SUBJECT)
  {
    $this->SUBJECT = $SUBJECT;
  }

  /**
   * Asigna el contenido del correo
   *
   * @param string $CONTENT contenido del correo
   * @return void
   *
   * Creacion 06 de Agosto del 2009
   * @author David Heredia
   */
  function set_content($CONTENT)
  {
    $this->CONTENT_HTML = $CONTENT;
  }

  /**
   * Verifica que los parametros sean correctos
   *
   * @return boolean true si los parametros son correctos y false si no lo son
   *
   * Creacion 06 de Agosto del 2009
   * @author David Heredia
   */
  function check_params()
  {
    if(!$this->FROM)
      return false;

    if(!count($this->TO))
      return false;

    if(!$this->SUBJECT)
      return false;

    return true;
  }

  /**
   * Agrega un archivo anexo al correo
   *
   * @param string $ATTACH ruta del archivo a anexar
   * @param string $FILENAME nombre con el que se desea que el archivo sea enviado
   * @return void
   *
   * Creacion 06 de Agosto del 2009
   * @author David Heredia
   */
  function set_attachment($ATTACH, $FILENAME = "")
  {
     if($FILENAME)
      $this->ATTACHMENTS[] = array($ATTACH, $FILENAME);
    else
      $this->ATTACHMENTS[] = $ATTACH;
  }

  /**
   * Envia el correo
   *
   * @return int cantidad de correos enviados
   *
   * Creacion 06 de Agosto del 2009
   * @author David Heredia
   */
  function send()
  {
    $MAIL_ATT = "";

    if(!$this->check_params())
      return false;

    $OB = "----=_OuterBoundary_000";
    $IB = "----=_InnerBoundery_001";

    //Headers para el correo
    $HEADER = "MIME-Version: 1.0\n";
    $HEADER .= "From: {$this->FROM}\n";
    //$HEADER .= "Reply-To: {$this->FROM}\n";
    $HEADER .= "Content-Type: multipart/mixed;\n\tboundary=\"".$OB."\"\n";
    $HEADER .= $this->HEADER;

    //Mensaje inicia con la alternativa entre text/html en OB
    $CONTENT  = "This is a multi-part message in MIME format.\n";
    $CONTENT .= "\n--".$OB."\n";
    $CONTENT .= "Content-Type: multipart/alternative;\n\tboundary=\"".$IB."\"\n\n";

    //Inicialización de contenido para HTML
    $CONTENT .= "\n--".$IB."\n";
    $CONTENT .= "Content-Type: text/html;\n\tcharset=\"utf-8\"\n\n";
    $CONTENT .= $this->CONTENT_HTML."\n\n";
    $CONTENT .= "\n--".$IB."--\n";

    //Attachments
    if($this->ATTACHMENTS)
    {
      foreach($this->ATTACHMENTS as $AttmFile)
      {
        if(is_array($AttmFile))
        {
          $FileName = $AttmFile[1];
          $AttmFile = $AttmFile[0];
        }
        else
        {
          $patharray = explode ("/", $AttmFile);
          $FileName = $patharray[count($patharray) - 1];
        }

        $MAIL_ATT .= $FileName.",";
        $FILE_TMP .= basename($AttmFile).",";
        $CONTENT .= "\n--" . $OB . "\n";
        $CONTENT .= "Content-Type: application/octetstream;\n\tname=\"" . $FileName . "\"\n";
        $CONTENT .= "Content-Transfer-Encoding: base64\n";
        $CONTENT .= "Content-Disposition: attachment;\n\tfilename=\"" . $FileName . "\"\n\n";

        $FileContent = file_get_contents($AttmFile);
        $FileContent = chunk_split(base64_encode($FileContent));
        $CONTENT .= $FileContent;
        $CONTENT .= "\n\n";

        $CONTENT_SAVE_LOG = str_replace($FileContent, "", $CONTENT);
      }
    }

    //Fin del mensaje
    $CONTENT .= "\n--" . $OB . "--\n";

    foreach($this->TO as $TO)
    {
      if(ini_get("safe_mode"))
        $SUCCESS = @mail($TO, $this->SUBJECT, $CONTENT, $HEADER);
      else
        $SUCCESS = @mail($TO, $this->SUBJECT, $CONTENT, $HEADER, "-f{$this->FROM}");

      if($SUCCESS)
        $SENT++;

      $MAIL_ATT = trim($MAIL_ATT, ',');
      $FILE_TMP = trim($FILE_TMP, ',');

    }

    return $SENT;
  }
}
?>