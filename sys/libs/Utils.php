<?php
abstract class Utils {

    public static function formatDateToHuman($date, $originalFormat)
    {
        if($date == '0000-00-00 00:00:00' || $date == '1969-12-31 18:00:00')
        {
            return 'N/A';
        }

        $parts = explode(' ', $date);
        $dateParts = explode('-', $parts[0]);

        if($originalFormat === 'dd/mm/yyyy')
        {
            return "{$dateParts[2]}/{$dateParts[1]}/{$dateParts[0]}";
        }

        if($originalFormat === 'dd/M/yyyy')
        {
            return "{$dateParts[2]}/" . self::monthNumberToName($dateParts[1]) ."/{$dateParts[0]}";
        }

        return '';
    }

    public static function formatDateTimeToHuman($date, $originalFormat = 'dd/M/yyyy')
    {
        if($date == '0000-00-00 00:00:00' || $date == '1969-12-31 18:00:00')
        {
            return 'N/A';
        }

        $parts = explode(' ', $date);
        $dateParts = explode('-', $parts[0]);
        $timeParts = explode(':', $parts[1]);

        if($originalFormat === 'dd/mm/yyyy')
        {
            return "{$dateParts[2]}/{$dateParts[1]}/{$dateParts[0]} {$timeParts[0]}:{$timeParts[1]}";
        }

        if($originalFormat === 'dd/M/yyyy')
        {
            return "{$dateParts[2]}/" . self::monthNumberToName($dateParts[1]) ."/{$dateParts[0]} {$timeParts[0]}:{$timeParts[1]}";
        }

        return '';
    }

    public static function formatDateToDatabase($date, $originalFormat)
    {
        $parts = explode(' ', $date);
        $dateParts = explode('/', $parts[0]);

        if($originalFormat === 'dd/mm/yyyy')
        {
            return "{$dateParts[2]}-{$dateParts[1]}-{$dateParts[0]} {$parts[1]}";
        }

        if($originalFormat === 'mm/dd/yyyy')
        {
            return "{$dateParts[2]}-{$dateParts[0]}-{$dateParts[1]} {$parts[1]}";
        }

        return '';
    }

    public static function monthNameToNumber($montName, $fullName = false)
    {
        if($fullName == 'true')
        {
            $months = array(
                'Enero' => '01',
                'Febrero' => '02',
                'Marzo' => '03',
                'Abril' => '04',
                'Mayo' => '05',
                'Junio' => '06',
                'Julio' => '07',
                'Agosto' => '08',
                'Septiembre' => '09',
                'Octubre' => '10',
                'Noviembre' => '11',
                'Diciembre' => '12'
            );

            return $months[$montName];
        }

        $months = array(
            'Ene' => '01',
            'Feb' => '02',
            'Mar' => '03',
            'Abr' => '04',
            'May' => '05',
            'Jun' => '06',
            'Jul' => '07',
            'Ago' => '08',
            'Sep' => '09',
            'Oct' => '10',
            'Nov' => '11',
            'Dic' => '12'
        );

        return $months[$montName];
    }

    public static function monthNumberToName($monthNumber, $fullName = false)
    {

        if($fullName == 'true')
        {
            $months = array(
                '01' => 'Enero',
                '02' => 'Febrero',
                '03' => 'Marzo',
                '04' => 'Abril',
                '05' => 'Mayo',
                '06' => 'Junio',
                '07' => 'Julio',
                '08' => 'Agosto',
                '09' => 'Septiembre',
                '10' => 'Octubre',
                '11' => 'Noviembre',
                '12' => 'Diciembre'
            );

            return $months[$monthNumber];
        }

        $months = array(
            '01' => 'Ene',
            '02' => 'Feb',
            '03' => 'Mar',
            '04' => 'Abr',
            '05' => 'May',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Ago',
            '09' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dic'
        );

        return $months[$monthNumber];
    }

    /**
     * Redirecciona a la ubicación recibida
     * 
     * @param string $sLocation
     */
    public static function redirect ($sLocation)
    {
        header("location: {$sLocation}");
        exit;
    }
    
    /**
     * Encripta el dato recibido en MD5
     * 
     * @param string $sDato
     * @return string
     */
    public static function encriptMD5($sDato)
    {
        return md5($sDato);
    }

    public static function formatPhone($phone)
    {
        if(!isset($phone{3})) { return ''; }
        $phone = preg_replace("/[^0-9]/", "", $phone);
        $length = strlen($phone);

        switch($length)
        {
            case 7:
                return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
            case 8:
                return preg_replace("/([0-9]{4})([0-9]{4})/", "$1-$2", $phone);
            case 10:
                return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
            case 11:
                return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1($2) $3-$4", $phone);
            default:
                return $phone;
        }
    }

    public static function parseJsonSelect($aData)
    {
        foreach($aData as $id => $text)
        {
            $aJsonSelect[] = compact('id', 'text');
        }

        return $aJsonSelect;
    }

    public static function validarCamposObligatorios($camposObligatorios, $informacion)
    {
        foreach($camposObligatorios as $campo => $etiqueta)
        {
            if(!trim($informacion[$campo]))
            {
                return "El campo $etiqueta es obligatorio y no se ha especificado";
            }
        }

        return true;
    }

    public static function create_dir($e_path)
    {
        if(file_exists($e_path))
        {
            return true;
        }

        mkdir($e_path);

        return true;
    }

   public static function get_files_dir($e_path, $a_extensions = array())
    {
        if(!file_exists($e_path) || !is_dir($e_path) || !is_readable($e_path))
        {
            return false;
        }

        $a_files = array();

        $handle = @opendir($e_path);

        while($file = @readdir($handle))
        {
            $a_info = pathinfo($file);

            if(@in_array($a_info['extension'], $a_extensions))
            {
                $ctime = filectime($e_path . $file);
                $a_files[$ctime] = $a_info['filename'] . '.' . $a_info['extension'];
            }
        }

        ksort($a_files);

        return $a_files;
    }

    public static function delete_file($sPathfile)
    {
        if(!file_exists($sPathfile))
        {
            return false;
        }

        @unlink($sPathfile);

        return true;
    }
}

/**
 * This file is part of the array_column library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Ben Ramsey (http://benramsey.com)
 * @license http://opensource.org/licenses/MIT MIT
 */
if (!function_exists('array_column')) {
    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();
        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }
        if (!is_array($params[0])) {
            trigger_error(
                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
                E_USER_WARNING
            );
            return null;
        }
        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }
        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }
        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;
        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }
        $resultArray = array();
        foreach ($paramsInput as $row) {
            $key = $value = null;
            $keySet = $valueSet = false;
            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }
            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }
            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }
        }
        return $resultArray;
    }
}

if (!function_exists('exif_imagetype')) {
    function exif_imagetype ( $filename )
    {
        if ( ( list($width, $height, $type, $attr) = getimagesize( $filename ) ) !== false ) {
            return $type;
        }

        return false;
    }
}