<?php
/**
 * Created by PhpStorm.
 * User: emmanuel
 * Date: 23/11/17
 * Time: 10:25
 */

require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('middlewareFolder').'Seguridad.php';
require_once $config->get('modelsFolder') . 'investigadores/Investigadores.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/AreasConocimiento.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/SubAreas.php';
require_once $config->get('modelsFolder') . 'videos/VideosEmprendimiento.php';
require_once $config->get('modelsFolder') . 'emprendedores/Emprendedores.php';
require_once $config->get('modelsFolder') . 'proyectosEmprendimiento/ProyectosEmprendimiento.php';
require_once $config->get('utilsFolder') . 'ResponseForm.php';

class AdminvideosemprendedoresController extends ControllerBase
{
    public function init()
    {
        Autentificar::validarLogin();
    }

    public function goAgregar()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'emprendedor/index' => 'Bienvenida',
            'Nuevo video'
        );

        $aEmprendedores = Emprendedores::obtenerEmprendedores();
        $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

        $this->_view->showSistemaMain('admin/emprendedores/videos/formulario.php', compact('aNavegacion', 'aSubAreasConocimiento', 'aEmprendedores'));
    }

    public function doAgregar()
    {
        $aForm = $this->_request['form'];

        if(!$nIdEmprendedor = $aForm['idEmprendedor'])
        {
            $nIdEmprendedor = Session::get('idEmprendedor');
        }

        //Se obtiene el id del video de Youtube
        parse_str(parse_url($aForm['liga_youtube'], PHP_URL_QUERY), $aYoutubeParams);

        $oResponse = new ResponseForm($aForm);

        //Si el video ya existe se agrega un error
        if($aVideo = VideosEmprendimiento::where(array("videos.`ON` = '1' AND (videos_emprendimiento.titulo = '{$aForm['titulo']}' OR videos_emprendimiento.liga_youtube = '{$aForm['liga_youtube']}')")))
        {
            $oResponse->addErrorMensaje('Error ya existe un video con el mismo título o liga de YouTube');

            $oErrors = $oResponse->getErrors();

            $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

            //Item activo del menú
            $activeItem = 'inicio';
            //Barra de navegación
            $aNavegacion = array(
                $this->_config->get('baseUrl').'admin/index' => 'Bienvenida',
                "Agregar video"
            );

            $this->_view->showSistemaMain('admin/emprendedores/videos/formulario.php', compact('oErrors', 'activeItem', 'aNavegacion', 'aSubAreasConocimiento'));
            return;
        }

        //Se obtiene el ID del area del conocimiento
        $aSubArea = SubAreas::where(array("`ON` = 1 AND id = {$aForm['id_subarea_conocimiento']}"), array('id_area_conocimiento'));

        $aVideo = array(
            'titulo' => $aForm['titulo'],
            'liga_youtube' => $aForm['liga_youtube'],
            'liga_resumen' => $aForm['liga_resumen'],
            'descripcion' => $aForm['descripcion'],
            'id_emprendedor' => $nIdEmprendedor,
            'id_area_conocimiento' => $aSubArea[0]['id_area_conocimiento'],
            'id_subarea_conocimiento' => $aForm['id_subarea_conocimiento'],
            'esquema_proteccion' => $aForm['esquema_proteccion'],
            'fecha_captura' => date('Y-m-d H:i:s'),
            'palabras_clave' => $aForm['palabras_clave'],
            'bVisible' => '1',
            'id_video_youtube' => $aYoutubeParams['v']
        );

        $nIdVideo = VideosEmprendimiento::agregarVideo($aVideo);

        ResponseForm::addFlashNotice('Se ha agregado el video correctamente'); //agregar mensajes

        if((Session::get('idPerfil')) == '1'){
            $this->_redirect($this->_config->get('baseUrl') . 'admin/emprendedores/videos/listado');
            return;

        }

        $this->_redirect($this->_config->get('baseUrl') . 'emprendedor/index');

    }

    public function goEditar()
    {
        $bActualizar = '1';
        $aSubAreasConocimiento = SubAreas::obtenerSubAreas();
        $activeItem = 'adminVideos';

        //Barra de navegación
        if((Session::get('idPerfil')) == '1')
        {
            $aNavegacion = array(
                $this->_config->get('baseUrl').'admin/index' => 'Bienvenida',
                "Actualizar video"
            );

        }else{
            $aNavegacion = array(
                $this->_config->get('baseUrl').'investigador/index' => 'Bienvenida',
                "Actualizar video"
            );
        }

        if(!$aVideo =  VideosEmprendimiento::obtenerInfoVideo($this->_request['video']))
        {
            $this->_redirect($this->_config->get('baseUrl') . 'emprendedor/index');
        }

        $this->_view->showSistemaMain('admin/emprendedores/videos/formulario.php', compact('bActualizar','aVideo','activeItem','aNavegacion','aSubAreasConocimiento' ));
    }

    public function doEditar()
    {
        $aForm = $this->_request['form'];
        $aForm['id'] = $this->_request['video'];
        $bActualizar = 1;
        $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

        $aForm['bVisible'] == '1' ? $aForm['bVisible'] = '0' : $aForm['bVisible'] = '1';

        //Barra de navegación
        if((Session::get('idPerfil')) == '1')
        {
            $aNavegacion = array(
                $this->_config->get('baseUrl').'admin/index' => 'Bienvenida',
                "Actualizar video"
            );

        }else{
            $aNavegacion = array(
                $this->_config->get('baseUrl').'investigador/index' => 'Bienvenida',
                "Actualizar video"
            );
        }

        if($nIdVideo = VideosEmprendimiento::agregarVideo($aForm))
        {
            $aVideo = VideosEmprendimiento::obtenerInfoVideo($nIdVideo);

            ResponseForm::addFlashNotice('Se ha editado el video correctamente');

            if((Session::get('idPerfil')) == '1'){
                $this->_view->showSistemaMain('admin/emprendedores/videos/formulario.php', compact('aNavegacion', 'aVideo', 'bActualizar', 'aSubAreasConocimiento'));
                return;
            }

            $this->_view->showSistemaMain('admin/emprendedores/videos/formulario.php', compact('aNavegacion','aVideo', 'bActualizar', 'aSubAreasConocimiento'));

        }
    }

    public function ajaxEliminar()
    {
        $nIdVideo = $this->_request['video'];

        $aResult = VideosEmprendimiento::eliminarVideo($nIdVideo);

        $this->_view->showJson($aResult);
    }

    public function goListado()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Administrar videos'
        );

        $this->_view->showSistemaMain('admin/emprendedores/videos/listado.php', compact('aNavegacion'));
    }

    public function obtenerJson()
    {
        $aProyectos = VideosEmprendimiento::obtenerVideos(Session::get('idEmprendedor'));

        $this->_view->showJson(array('data' => $aProyectos));
    }

}