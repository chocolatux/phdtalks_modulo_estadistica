<?php

require_once $config->get('utilsFolder') . 'ResponseForm.php';
require_once $config->get('modelsFolder') . 'videos/Videos.php';
require_once $config->get('middlewareFolder').'Seguridad.php';
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('modelsFolder') . 'investigadores/Investigadores.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/AreasConocimiento.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/SubAreas.php';

class AdminvideosinvestigadoresController extends ControllerBase{

    public function init()
    {
        //Quitar comentarios!!!!!!!!
        //Autentificar::validarLogin();
    }

    public function goAgregar()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'investigador/index' => 'Bienvenida',
            'Nuevo video'
        );

        $aInvestigadores = Investigadores::obtenerInvestigadores();
        $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

        $this->_view->showSistemaMain('admin/videos/formulario.php', compact('aNavegacion', 'aSubAreasConocimiento', 'aInvestigadores'));
    }

    public function ajaxAgregarVideo()
    {
        $aForm = $this->_request['form'];

        if(!$nIdInvestigador = $aForm['idInvestigador'])
        {
            $nIdInvestigador = Session::get('idInvestigador');
        }

        //Si el video ya existe se agrega un error
        if($aVideo = Videos::where(array("videos.`ON` = '1' AND (videos.titulo = '{$aForm['titulo']}')")))
        {
            //Se regresa un error
            $this->_view->showJson(array('success' => false, 'msg' => 'Error ya existe un vídeo con el mismo nombre en la base de datos'));
            return;
        }

        //Se obtiene el ID del area del conocimiento
        $aSubArea = SubAreas::where(array("`ON` = 1 AND id = {$aForm['id_subarea_conocimiento']}"), array('id_area_conocimiento'));

        $aVideo = array(
            'titulo' => $aForm['titulo'],
            'liga_articulo' => $aForm['liga_articulo'],
            'descripcion' => $aForm['descripcion'],
            'id_investigador' => $nIdInvestigador,
            'id_area_conocimiento' => $aSubArea[0]['id_area_conocimiento'],
            'id_subarea_conocimiento' => $aForm['id_subarea_conocimiento'],
            'fecha_captura' => date('Y-m-d H:i:s'),
            'palabras_clave' => $aForm['palabras_clave'],
            'coautores' => $aForm['coautores'],
            'journal' => $aForm['journal'],
            'visible' => '0',
            '`ON`' => '0'
        );

        //Se agrega el video a la base de datos
        if($nIdVideo = Videos::agregarVideo($aVideo))
        {
            $this->_view->showJson(array('success' => true, 'idVideo' => $nIdVideo));
            return;
        }

    }

    public function ajaxValidarSiVideoExiste()
    {
        $sTituloVideo = $this->_request['video'];

        //Si el video ya existe se agrega un error
        if(!$aVideo = Videos::where(array("videos.`ON` = '1' AND (videos.titulo = '{$sTituloVideo}')")))
        {
            //Se regresa true
            $this->_view->showJson(array('success' => true));
            return;
        }

        //Se regresa un error
        $this->_view->showJson(array('success' => false, 'msg' => 'Error ya existe un video con el mismo titulo'));
        return;
    }

    public function doAgregar()
    {
        $aForm = $this->_request['form'];

        if(!$nIdInvestigador = $aForm['idInvestigador'])
        {
            $nIdInvestigador = Session::get('idInvestigador');
        }

        parse_str(parse_url($aForm['liga_youtube'], PHP_URL_QUERY), $aYoutubeParams);

        $oResponse = new ResponseForm($aForm);

        //Si el video ya existe se agrega un error
        if($aVideo = Videos::where(array("videos.`ON` = '1' AND (videos.titulo = '{$aForm['titulo']}' OR videos.liga_youtube = '{$aForm['liga_youtube']}')")))
        {
            $oResponse->addErrorMensaje('Error ya existe un video con el mismo título o liga de YouTube');

            $oErrors = $oResponse->getErrors();

            $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

            //Item activo del menú
            $activeItem = 'inicio';
            //Barra de navegación
            $aNavegacion = array(
                $this->_config->get('baseUrl').'admin/index' => 'Bienvenida',
                "Agregar video"
            );

            $this->_view->showSistemaMain('admin/videos/formulario.php', compact('oErrors', 'activeItem', 'aNavegacion', 'aSubAreasConocimiento'));

            return;
        }

        if(!$aYoutubeParams['v'])
        {
            $oResponse->addErrorMensaje('Error, el link de youtube no es válido');

            if(Session::get('idPerfil') == 1)
            {
                $this->_redirect($this->_config->get('baseUrl') . 'admin/video/agregar');
            }

            $this->_redirect($this->_config->get('baseUrl') . 'investigador/videos/agregar');

            return;
        }

        //Se obtiene el ID del area del conocimiento
        $aSubArea = SubAreas::where(array("`ON` = 1 AND id = {$aForm['id_subarea_conocimiento']}"), array('id_area_conocimiento'));

        $aVideo = array(
            'titulo' => $aForm['titulo'],
            'liga_youtube' => $aForm['liga_youtube'],
            'liga_articulo' => $aForm['liga_articulo'],
            'descripcion' => $aForm['descripcion'],
            'id_investigador' => $nIdInvestigador,
            'id_area_conocimiento' => $aSubArea[0]['id_area_conocimiento'],
            'id_subarea_conocimiento' => $aForm['id_subarea_conocimiento'],
            'fecha_captura' => date('Y-m-d H:i:s'),
            'palabras_clave' => $aForm['palabras_clave'],
            'coautores' => $aForm['coautores'],
            'journal' => $aForm['journal'],
            'visible' => '1',
            'id_video_youtube' => $aYoutubeParams['v']
        );

        $nIdVideo = Videos::agregarVideo($aVideo);

        ResponseForm::addFlashNotice('Se ha agregado el video correctamente'); //agregar mensajes

        if((Session::get('idPerfil')) == '1'){
            $this->_redirect($this->_config->get('baseUrl') . 'admin/videos/listado');
            return;
        }

        $this->_redirect($this->_config->get('baseUrl') . 'investigador/index');
    }

    public function goEditar()
    {
        $bActualizar = '1';

        $aSubAreasConocimiento = SubAreas::obtenerSubAreas();
        $activeItem = 'adminVideos';

        if((Session::get('idPerfil')) == '1')
        {
            $aNavegacion = array(
                $this->_config->get('baseUrl').'admin/index' => 'Bienvenida',
                "Actualizar video"
            );

        }else{
            $aNavegacion = array(
                $this->_config->get('baseUrl').'investigador/index' => 'Bienvenida',
                "Actualizar video"
            );
        }

        if(!$aVideo =  Videos::obtenerInfoVideo($this->_request['video']))
        {
            $this->_redirect($this->_config->get('baseUrl') . 'investigador/index');
        }

        $this->_view->showSistemaMain('admin/videos/formulario.php', compact('bActualizar','aVideo','activeItem','aNavegacion','aSubAreasConocimiento' ));
    }

    public function doEditar()
    {
        $aForm = $this->_request['form'];
        $aForm['id'] = $this->_request['video'];
        $bActualizar = 1;
        $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

        $aForm['visible'] == '1' ? $aForm['visible'] = '0' : $aForm['visible'] = '1';

        //Barra de navegación
        if((Session::get('idPerfil')) == '1')
        {
            $aNavegacion = array(
                $this->_config->get('baseUrl').'admin/index' => 'Bienvenida',
                "Actualizar video"
            );

        }else{
            $aNavegacion = array(
                $this->_config->get('baseUrl').'investigador/index' => 'Bienvenida',
                "Actualizar video"
            );
        }

        if($nIdVideo = Videos::agregarVideo($aForm))
        {
            $aVideo = Videos::obtenerInfoVideo($nIdVideo);

            ResponseForm::addFlashNotice('Se ha editado el video correctamente');

            if((Session::get('idPerfil')) == '1')
            {
                $this->_view->showSistemaMain('admin/videos/formulario.php', compact('aNavegacion','aVideo', 'bActualizar', 'aSubAreasConocimiento'));
                return;
            }

            $this->_view->showSistemaMain('admin/videos/formulario.php', compact('aNavegacion','aVideo', 'bActualizar', 'aSubAreasConocimiento'));
        }
    }

    public function ajaxEliminar()
    {
        $nIdVideo = $this->_request['video'];

        /*Se elimina el video*/
        $aResult = Videos::eliminarVideo($nIdVideo);

        $this->_view->showJson($aResult);
    }

    public function goListado()
    {
        //Barra de navegación
        $aNavegacion = array(
            $this->_config->get('baseUrl') . 'admin/index' => 'Bienvenida',
            'Administrar videos'
        );

        $this->_view->showSistemaMain('admin/videos/listado.php', compact('aNavegacion'));
    }

    public function obtenerJson()
    {
        $aVideos = Videos::obtenerVideos(Session::get('idInvestigador'));

        $this->_view->showJson(array('data' => $aVideos));
    }

    //Eliminar!"!!!!!!
    public function response()
    {
        $sResponse = $this->_request['response'];

        echo($sResponse);

        Videos::eliminarVideo('1');
        $this->_view->showJson(array('response' => $sResponse));
    }

}