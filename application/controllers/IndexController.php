<?php
/**
 * Clase IndexController
 * 
 * La clase manipula las acciones del index
 * 
 * Creado 7/Abril/2017
 * 
 * @category Class
 * @package Controllers
 * @author David Heredia <david@chocolatux.com>
 */
require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('middlewareFolder').'Seguridad.php';
require_once $config->get('modelsFolder') . 'videos/Videos.php';
require_once $config->get('modelsFolder').'areasConocimiento/AreasConocimiento.php';

class IndexController extends ControllerBase {

    public function init()
    {

    }

    public function goIndex()
    {
        //Obtener videos recientes
        $data['aVideosRecientes'] = Videos::obtenerVideosRecientes();
        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        $this->_view->showMain('inicio.php', $data);
    }

    public function goAcercaDe()
    {
        $data['bFlag'] = 1;

        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        $this->_view->showMain('acercaDe.php', $data);

    }

    public function goServicios()
    {
        $this->_view->showMain('servicios.php');
    }


    public function goAvisoPrivacidad()
    {
        $data['bFlag'] = 1;

        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        $this->_view->showMain('avisoPrivacidad.php', $data);

    }

    public function goAvisoLegal()
    {
        $data['bFlag'] = 1;

        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        $this->_view->showMain('avisoLegal.php', $data);

    }

    public function goTutoriales()
    {
        $data['bFlag'] = 1;

        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        $this->_view->showMain('tutoriales.php', $data);

    }
}