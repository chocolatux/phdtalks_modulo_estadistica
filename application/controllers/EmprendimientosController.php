<?php
/**
 * Created by PhpStorm.
 * User: emmanuel
 * Date: 09/11/17
 * Time: 13:52
 */
require_once $config->get('modelsFolder') . 'videos/VideosEmprendimiento.php';
require_once $config->get('modelsFolder').'areasConocimiento/AreasConocimiento.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/SubAreas.php';
require_once $config->get('utilsFolder') . 'ResponseForm.php';
require_once $config->get('modelsFolder') . 'emprendedores/Emprendedores.php';
require_once $config->get('modelsFolder') . 'usuarios/UsuUsuarios.php';
require_once $config->get('modelsFolder') . 'proyectosEmprendimiento/ProyectosEmprendimiento.php';
require_once $config->get('modelsFolder') . 'registroVisitas/RegistroVisitas.php';


class EmprendimientosController extends ControllerBase
{
    public function init()
    {

    }

    private function contadorVisitas($nCantidadVisitas, $nIdVideo, $nClicsArticulo)
    {
        $nCantidadVisitas += 1;

        $aVideo = array(
            'id' => $nIdVideo,
            'visitas' => $nCantidadVisitas
        );

        $aRegistro = array(
            'id_video' => $nIdVideo,
            'fecha_hora' => date('Y-m-d H:i:s'),
            //La variable tipo hace referencia al tipo de registro 1 para videos y 2 para articulos
            'tipo' => 1
        );

        if(isset($nClicsArticulo))
        {
            $nClicsArticulo += 1;
            $aVideo['clics_articulo'] = $nClicsArticulo;
            //La variable tipo hace referencia al tipo de registro 1 para videos y 2 para articulos
            $aRegistro['tipo'] = 2;
        }

        VideosEmprendimiento::agregarVideo($aVideo);

        if($nIdUsuario = Session::get('idUsuario'))
        {
            $aRegistro['id_usuario'] = $nIdUsuario;
        }

        //Se crea el registro de la visita
        RegistroVisitas::agregarRegistro($aRegistro);

    }

    public function goArticulo()
    {
        $nIdVideo = $this->_request['id'];
        $aVideo = VideosEmprendimiento::obtenerInfoVideo($nIdVideo);
        $this->contadorVisitas($aVideo['visitas'], $nIdVideo, $aVideo['clics_articulo']);
        $this->_redirect($aVideo['liga_resumen']);
    }

    public function goRegistro()
    {
        $data['bEmprendimiento'] = '1';

        $this->_view->showMain('registroEmprendedor.php', $data);
    }

    public function doRegistro()
    {
        $oResponse = new ResponseForm();

        //formulario
        $aForm = $this->_request['form'];

        //Si el emprendedor ya existe se agrega un error
        if(Emprendedores::where(array("`ON` = 1 AND correo = '{$aForm['correo']}'")))
        {
            $oResponse->addErrorMensaje('Error ya existe un emprendedor registrado con el mismo correo');

            $oErrors = $oResponse->getErrors();

            $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

            $this->_view->showMain('registroEmprendedor.php', compact('oErrors', 'aSubAreasConocimiento'));

            return;
        }

        //registrar nuevo investigador
        if($nIdEmprendedor = Emprendedores::agregarEmprendedor($aForm))
        {

            $aDatosUsuario = array(
                'idEmprendedor' => $nIdEmprendedor,
                'idPerfil' => 3,
                'nombre' => $aForm['nombre'],
                'email' => $aForm['correo'],
                'password' => $aForm['password']
            );

            $oUsuario = UsuUsuarios::agregarUsuario($aDatosUsuario);

            $aDatosProyecto = array(
                'idEmprendedor' => $nIdEmprendedor,
                'nombre' => $aForm['nombre_proyecto'],
                'bEmpresaConstituida' => $aForm['bEmpresaConstituida'],
                'pais' => $aForm['pais'],
                'estado' => $aForm['estado'],
                'ciudad' => $aForm['ciudad'],
                'correo_contacto' => $aForm['correo_contacto'],
                'nombre_integrantes' => $aForm['nombre_integrantes'],
                'descripcion_producto' => $aForm['descripcion_producto'],
                'descripcion_base' => $aForm['descripcion_base']
            );

            ProyectosEmprendimiento::agregarProyectoEmprendimiento($aDatosProyecto);

            ResponseForm::addFlashNotice('Tu cuenta ha sido creada exitosamente ingresa tus datos para iniciar sesión');

            $this->_redirect($this->_config->get('baseUrl') . 'login');

        }

    }


    public function goIndex()
    {
        $data['bEmprendimiento'] = '1';

        $data['aVideosRecientes'] = VideosEmprendimiento::obtenerVideosRecientesEmprendimiento();
        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();
        $this->_view->showMain('inicioEmprendedores.php', $data);
    }

    public function ajaxObtenerVideosEmprendedores()
    {
        //La cantidad de videos que se desea mostrar por bloque
        $nCantidadVideos = 9;

        //Se obtiene el número del primer video
        $nPaginaActual = $this->_request['p'];

        $nVideoInicial = ($nPaginaActual * $nCantidadVideos);

        $aVideosRecientes = VideosEmprendimiento::obtenerVideosRecientesEmprendimiento($nVideoInicial);

        $this->_view->showJson($aVideosRecientes);
    }

    public function goBuscarVideos()
    {
        $sPalabra = $this->_request['r'];

        $data['bEmprendimiento'] = '1';

        //Se obtienen las categorias para el menú
        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        //Se obtienen los videos que coincidan con la palabra
        $data['busqueda'] = VideosEmprendimiento::buscarVideo($sPalabra);
        //$data['busqueda']['areas'] = Videos::obtenerInfoVideo($data['busqueda']['videos']);
        $data['busqueda']['palabra'] = $sPalabra;

        $this->_view->showMain('videos/buscar.php', $data);
    }

    public function goVisualizarVideo()
    {
        $nIdVideo = $this->_request['id'];

        $data['bEmprendimiento'] = '1';
        $data['aInfoVideo'] = VideosEmprendimiento::obtenerInfoVideo($nIdVideo);
        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();


        //Contador de visitas
        $this->contadorVisitas($data['aInfoVideo']['visitas'], $nIdVideo);

        $this->_view->showMain('videos/visualizacion.php', $data);

    }

    public function goVideosEmprendimientoArea()
    {
        $nIdArea = $this->_request['id'];
        $data['bEmprendimiento'] = '1';

        //Obtener videos
        $data['aVideosAreas'] = VideosEmprendimiento::obtenerVideosPorArea($nIdArea);

        $data['areasConocimiento'] = AreasConocimiento::obtenerAreasConocimiento();

        foreach($data['areasConocimiento'] as $aAreasConocimiento)
        {
            if($nIdArea == $aAreasConocimiento['id'])
            {
                $data['sAreaConocimiento'] = $aAreasConocimiento['nombre'];
            }
        }

        $this->_view->showMain('videos/videosPorArea.php', $data);
    }

}