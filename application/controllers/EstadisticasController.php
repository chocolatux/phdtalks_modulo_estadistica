<?php

require_once $config->get('modelsFolder') . 'estadisticas/estadisticas.php';

class EstadisticasController extends ControllerBase{

  public function init()
  {

  }

  public function obtenerSeguidoresMensuales(){
    $QueryResult = Array(1=>Estadisticas::obtenerSeguidores(1)[0]["seguidores"],
    2=>Estadisticas::obtenerSeguidores(2)[0]["seguidores"],
    3=>Estadisticas::obtenerSeguidores(3)[0]["seguidores"],
    4=>Estadisticas::obtenerSeguidores(4)[0]["seguidores"],
    5=>Estadisticas::obtenerSeguidores(5)[0]["seguidores"],
    6=>Estadisticas::obtenerSeguidores(6)[0]["seguidores"],
    7=>Estadisticas::obtenerSeguidores(7)[0]["seguidores"],
    8=>Estadisticas::obtenerSeguidores(8)[0]["seguidores"],
    9=>Estadisticas::obtenerSeguidores(9)[0]["seguidores"],
    10=>Estadisticas::obtenerSeguidores(10)[0]["seguidores"],
    11=>Estadisticas::obtenerSeguidores(11)[0]["seguidores"],
    12=>Estadisticas::obtenerSeguidores(12)[0]["seguidores"]
    );
    $this->_view->showJson($QueryResult);
  }

  public function obtenerPublicacionesGenerales(){
    $mes = $this->_request['m'];
    $QueryResult = Array(1=>Estadisticas::obtenerPublicacionesGenerales(1)[0]["COUNT(videoID)"],
    2=>Estadisticas::obtenerPublicacionesGenerales(2)[0]["COUNT(videoID)"],
    3=>Estadisticas::obtenerPublicacionesGenerales(3)[0]["COUNT(videoID)"],
    4=>Estadisticas::obtenerPublicacionesGenerales(4)[0]["COUNT(videoID)"],
    5=>Estadisticas::obtenerPublicacionesGenerales(5)[0]["COUNT(videoID)"],
    6=>Estadisticas::obtenerPublicacionesGenerales(6)[0]["COUNT(videoID)"],
    7=>Estadisticas::obtenerPublicacionesGenerales(7)[0]["COUNT(videoID)"],
    8=>Estadisticas::obtenerPublicacionesGenerales(8)[0]["COUNT(videoID)"],
    9=>Estadisticas::obtenerPublicacionesGenerales(9)[0]["COUNT(videoID)"],
    10=>Estadisticas::obtenerPublicacionesGenerales(10)[0]["COUNT(videoID)"],
    11=>Estadisticas::obtenerPublicacionesGenerales(11)[0]["COUNT(videoID)"],
    12=>Estadisticas::obtenerPublicacionesGenerales(12)[0]["COUNT(videoID)"]
    );
    $this->_view->showJson($QueryResult);
  }

  public function obtenerVistasMensuales(){
    $QueryResult = Array(1=>Estadisticas::obtenerVistasGenerales(2)[0]["SUM(vistas)"],
    2=>Estadisticas::obtenerVistasGenerales(2)[0]["SUM(vistas)"],
    3=>Estadisticas::obtenerVistasGenerales(3)[0]["SUM(vistas)"],
    4=>Estadisticas::obtenerVistasGenerales(4)[0]["SUM(vistas)"],
    5=>Estadisticas::obtenerVistasGenerales(5)[0]["SUM(vistas)"],
    6=>Estadisticas::obtenerVistasGenerales(6)[0]["SUM(vistas)"],
    7=>Estadisticas::obtenerVistasGenerales(7)[0]["SUM(vistas)"],
    8=>Estadisticas::obtenerVistasGenerales(8)[0]["SUM(vistas)"],
    9=>Estadisticas::obtenerVistasGenerales(9)[0]["SUM(vistas)"],
    10=>Estadisticas::obtenerVistasGenerales(10)[0]["SUM(vistas)"],
    11=>Estadisticas::obtenerVistasGenerales(11)[0]["SUM(vistas)"],
    12=>Estadisticas::obtenerVistasGenerales(12)[0]["SUM(vistas)"]
    );
    $this->_view->showJson($QueryResult);
  }


}

 ?>
