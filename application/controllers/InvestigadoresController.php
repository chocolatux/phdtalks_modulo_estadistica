<?php

require_once $config->get('middlewareFolder').'Autentificar.php';
require_once $config->get('utilsFolder') . 'ResponseForm.php';
require_once $config->get('modelsFolder') . 'investigadores/Investigadores.php';
require_once $config->get('modelsFolder') . 'usuarios/UsuUsuarios.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/AreasConocimiento.php';
require_once $config->get('modelsFolder') . 'areasConocimiento/SubAreas.php';
/**
 * Clase InvestigadoresController
 *
 * La clase manipula las acciones de los Investigadores del sistema
 *
 * Creado 17/Abril/2017
 *
 * @category Class
 * @package Controllers
 * @author Emmanuel
 */

class InvestigadoresController extends ControllerBase{

    public function init()
    {

    }

    public function goRegistro()
    {
        $this->_view->showMain('registroInvestigador.php');
    }

    public function doRegistro()
    {
        $oResponse = new ResponseForm();

        //formulario
        $aForm = $this->_request['form'];

        //Si el investigador ya existe se agrega un error
        if(Investigadores::where(array("`ON` = 1 AND correo_personal = '{$aForm['correo_personal']}'")))
        {
            $oResponse->addErrorMensaje('Error ya existe un Investigador registrado con el mismo correo');

            $oErrors = $oResponse->getErrors();

            $aSubAreasConocimiento = SubAreas::obtenerSubAreas();

            //Item activo del menú
            $activeItem = 'inicio';

            //Barra de navegación
            $aNavegacion = array(
                $this->_config->get('baseUrl').'investigador/index' => 'Bienvenida',
                "Agregar video"
            );

            $this->_view->showMain('investigadores/formulario.php', compact('oErrors', 'activeItem', 'aNavegacion', 'aSubAreasConocimiento'));
            return;

        }

        $aForm['fecha_nac'] = Utils::formatDateToDatabase($aForm['fecha_nac'], 'dd/mm/yyyy');

        //registrar nuevo investigador
        if($nIdInvestigador = Investigadores::agregarInvestigador($aForm))
        {

            $aDatosUsuario = array(
                'idInvestigador' => $nIdInvestigador,
                'idPerfil' => 2,
                'nombre' => $aForm['nombre'],
                'email' => $aForm['correo_personal'],
                'password' => $aForm['password']
            );

            $oUsuario = UsuUsuarios::agregarUsuario($aDatosUsuario);

            ResponseForm::addFlashNotice('Tu cuenta ha sido creada exitosamente ingresa tus datos para iniciar sesión');

            $this->_redirect($this->_config->get('baseUrl') . 'login');

        }

    }


}
