<?php
/**
 * Clase Autentificar
 * La clase contiene fnciones utilizadas para la validación
 * de datos
 * 
 * Creado 07/Abril/2017
 * 
 * @category Class
 * @package Middleware
 * @author David Heredia <david@chocolatux.com>
 */
require_once $config->get('modelsFolder').'usuarios/UsuUsuarios.php';
require_once $config->get('modelsFolder').'seguridad/SegPermisos.php';

class Autentificar {
    
    /**
     * Valida si se tiene una sesión iniciada
     * 
     * @return bool
     */
    public static function tieneLogin()
    {
        if(!Session::get('idUsuario', false))
        {
            
            return false;
        }
        
        return true;
    }
    
    /**
     * Valida si se tiene una sesión iniciada
     * en caso de no tenerlalo redirige al login
     * 
     * @return bool
     */
    public static function validarLogin()
    {
        if(!self::tieneLogin())
        {
            Utils::redirect(Config::getInstance()->get('baseUrl') . "login");
        }
    }
    
    /**
     * Reliza el login de un usuario
     * 
     * @param string $sUsuario
     * @param string $sPassword
     * @return boolean
     */
    public static function login($sUsuario, $sPassword)
    {
        //Bandera de control del resultado
        $bResult = false;

        //Filtros para buscar el usuario
        $aWhere = array(
            "`ON` = '1'",
            "AND email = '{$sUsuario}'",
            "AND password = '" . Utils::encriptMD5($sPassword) . "'"
        );

        //Se busca el usuario
        if(!$oUsuUsuario = UsuUsuarios::find($aWhere))
        {
            //En caso de no encontrar coincidencias
            return $bResult;

        }

        //Si se encuentra una coincidencia se obtiene
        if(($oUsuUsuario->usuario == $sUsuario || $oUsuUsuario->email == $sUsuario) && $oUsuUsuario->password == Utils::encriptMD5($sPassword))
        {
            //Se obtiene la información del usuario
            $aDatosUsuario = $oUsuUsuario->obtenerUsuarioInfo($oUsuUsuario->ID);

            //Se obtienen los permisos del perfil del usuario
            $aPermisos = SegPermisos::obtenerPermisosPerfil($aDatosUsuario['idPerfil']);

            //Se agregan las variables de sesión
            Session::add('idUsuario', $oUsuUsuario->ID);
            Session::add('idPerfil', $oUsuUsuario->idPerfil);
            Session::add('idInvestigador', $oUsuUsuario->idInvestigador);
            Session::add('nombre', $aDatosUsuario['nombre']);
            Session::add('perfil', $aDatosUsuario['perfil']);
            Session::add('permisos', $aPermisos);
            Session::add('idEmprendedor', $oUsuUsuario->idEmprendedor);
            $bResult = true;
        }

        return $bResult;
    }
    
    /**
     * 
     */
    public static function logout()
    {
        Session::close();
    }
}
