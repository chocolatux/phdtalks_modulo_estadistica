<div id="menuVideos" class="ui borderless menu">
    <div class="item">
        <p class="txtAzul">Filtrar videos por:</p>
    </div>

    <a href="<?echo($bEmprendimiento == '1' ? $config->get('baseUrl') . 'emprendimiento' : '')?>" class="item">
        <div class="ui mini image">
            <img src="<?echo($config->get('baseUrl'))?>assets/img/clock.png">
        </div>
        <div class="middle aligned content txtAzul bordeAzul">
            Videos más recientes
        </div>
    </a>

    <div id="menuAreas" class="ui menu transparente">
        <div class="ui dropdown item">
            <div class="ui mini image">
                <img src="<?echo($config->get('baseUrl'))?>assets/img/areas.png">
            </div>
            <div class="middle aligned content txtAzul bordeAzul">
                Áreas del Conocimiento
            </div>
            <i class="dropdown icon"></i>
            <div id="dropdownAreas" class="menu">
                <?foreach($areasConocimiento as $area):?>
                    <a href="<?echo($bEmprendimiento == '1' ? $config->get('baseUrl') . 'emprendimiento/videos/area?id=' . $area['id'] : $config->get('baseUrl') . 'videos/area?id=' . $area['id'])?>" class="item"><? echo $area['nombre'];?></a>
                <?endforeach;?>
            </div>
        </div>
    </div>

    <div id="menuEmprendimiento" class="ui right menu transparente">
        <a href="<?echo($config->get('baseUrl'))?>" id="itemEmprendimiento" class="item">
            <div class="ui mini image">
                <img src="<?echo($config->get('baseUrl'))?>assets/img/videoResumenes.png">
            </div>
            <p class="txtAzul">Video-resúmenes de papers</p>
        </a>
    </div>
</div>

<div id="contenido2">
    <?$aIdVideo = explode("=", $aInfoVideo['liga_youtube'])?>
    <div class="ui grid stackable">
        <div class="row">
            <div class="column">
                <p class="txtTitulo">
                    <?echo($aInfoVideo['titulo'])?>
                </p>

                <div id="datosVideos">
                    <p class="txtSubcribe"><span class="colorTitulo"><?echo($bEmprendimiento == '1' ? 'Emprendedor:' : 'Investigador:')?></span> <?echo($aInfoVideo['nombre']) . ' ' . $aInfoVideo['apellido_p'] . ' ' . $aInfoVideo['apellido_m']?> <br>
                        <span class="colorTitulo">Disciplina:</span> <?echo($aInfoVideo['area'])?> > <?echo($aInfoVideo['subArea'])?><br>
                        <?if($aInfoVideo['journal']):?><span class="colorTitulo">Journal:</span> <?echo($aInfoVideo['journal'])?><br><?endif;?>
                        <?if($aInfoVideo['coautores']):?><span class="colorTitulo">Coautores:</span> <?echo($aInfoVideo['coautores'])?><br><?endif;?>

                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="eight wide column">
                <div class="video-responsive">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?echo($aIdVideo[1])?>?autoplay=1" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                </div>
            </div>

            <div class="six wide column">
                <div id="datosVideos">
                    <p class="txtSubcribe"><span class="colorTitulo">Resumen:</span>
                        <p><?echo($aInfoVideo['descripcion']);?></p>
                </div>
                <?if($aInfoVideo['liga_articulo']){?>
                    <a href="<?echo($bEmprendimiento != '1' ? $config->get('baseUrl') . 'articulo?id=' . $aInfoVideo['id'] : $config->get('baseUrl') . 'articuloEmprendedor?id=' . $aInfoVideo['id'])?>" target="_blank">
                        <div id="btnPHD">
                            <button id="buttonPHD" type="submit" class="ui basic button centered">
                                <p class="txtButton"> Ir al artículo</p>
                            </button>
                        </div>
                    </a>
                <?}elseif($aInfoVideo['liga_resumen']){?>
                    <a href="<?echo($config->get('baseUrl'))?>articuloEmprendedor?id=<?echo($aInfoVideo['id'])?>" target="_blank">
                        <div id="btnPHD">
                            <button id="buttonPHD" type="submit" class="ui basic button centered">
                                <p class="txtButton"> Ir al resumen</p>
                            </button>
                        </div>
                    </a>
                <?}?>
            </div>
        </div>
    </div>
</div>