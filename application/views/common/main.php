<!DOCTYPE html>
<html>
    <head>
        <title>PHD TALKS</title>

        <!-- Standard Meta -->
        <meta charset="utf-8"/> <!--caracteres en espanol-->
        <meta http-equiv="X-UA-COMPATIBLE" content="IE=edge, chrome"/> <!--que sea compatible con las nuevas caracteristicas-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum=1.0">
        <meta name="keywords" content=""> <!--motores de busqueda-->

        <link rel="icon" href="<?echo($config->get('baseUrl'))?>assets/img/favicon.ico" type="image/x-icon" />

        <!--Site Properties-->
        <link rel="stylesheet" type="text/css" href="<?echo($config->get('baseUrl'))?>assets/css/semantic.min.css"> <!--calendario-->
        <link rel="stylesheet" type="text/css" href="<?echo($config->get('baseUrl'))?>assets/css/style.css"> <!--estilo de la hoja-->

        <script src="<?echo($config->get('baseUrl'))?>assets/js/jquery.min.js"></script> <!--libreria de Jquery-->
        <script src="<?echo($config->get('baseUrl'))?>assets/js/semantic.min.js"></script> <!--calendario-->

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-106100280-1', 'auto');
            ga('send', 'pageview');
        </script>

        <script>
            // namespace
            window.semantic = {
                handler: {}
            };

            semantic.menu = {};

            // ready event
            semantic.menu.ready = function() {

                // selector cache
                var
                    $dropdownItem = $('.menu .dropdown .item'),
                    $menuItem     = $('.menu a.item, .menu .link.item').not($dropdownItem),
                    $dropdown     = $('.menu .ui.dropdown'),
                // alias
                    handler = {
                        activate: function() {
                            if(!$(this).hasClass('dropdown browse')) {
                                $(this)
                                    .addClass('active')
                                    .closest('.ui.menu')
                                    .find('.item')
                                    .not($(this))
                                    .removeClass('active');
                            }
                        }
                    };

                $dropdown.dropdown({
                    on: 'hover'
                });
            };

            $(window).load(function () {
                $('#preloader').fadeOut('slow');
            });
        </script>
    </head>

    <body>
        <div id="home">
            <div id="header">
                <? require_once('header.php');?>
            </div>

            <div id="contenedor">
                <?php $this->show($sSectionFile, get_defined_vars()['vars'])?>
            </div>

            <div id="footer" class="ui vertical segment">
                <?require_once('footer.php');?>
            </div>
        </div>

        <script>
            $(document).ready(function() {

                window.semantic = {
                    handler: {}
                };

                semantic.menu = {};


                semantic.menu.ready = function() {

                    // selector cache
                    var
                        $dropdownItem = $('.menu .dropdown .item'),
                        $menuItem     = $('.menu a.item, .menu .link.item').not($dropdownItem),
                        $dropdown     = $('.menu .ui.dropdown'),
                        // alias
                        handler = {
                            activate: function() {
                                if(!$(this).hasClass('dropdown browse')) {
                                    $(this)
                                        .addClass('active')
                                        .closest('.ui.menu')
                                        .find('.item')
                                        .not($(this))
                                        .removeClass('active');
                                }
                            }
                        };

                    $dropdown.dropdown({
                        on: 'hover'
                    });
                };

                $(window).load(function () {
                    $('#preloader').fadeOut('slow');
                });


            });
        </script>
    </body>
</html>