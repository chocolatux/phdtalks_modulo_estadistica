<div id="fixedmenu" class="ui top fixed">
    <div id="barraMenuAdmin" class="ui borderless main menu">
        <div class="ui small image">
            <a href="<?echo($config->get('baseUrl'))?>">
                <img src="<?echo($config->get('baseUrl'))?>assets/img/logo.png">
            </a>
        </div>

        <div id="dropdownMenu" class="right menu">
            <div class="ui simple dropdown item dropSimpleMenu">
                <i class="ui icon large user"></i>
                <p class="nomUsuarioPHD"><?echo(Session::get('nombre')); if(Session::get('idPerfil') == '1'): echo(' (Administrador)'); elseif(Session::get('idPerfil') == '2'): echo(' (Investigador)'); elseif(Session::get('idPerfil') == '3'): echo(' (Emprendedor)'); endif;?></p>
                <i id="flechaDropdown" class="dropdown icon"></i>
                <div class="ui secondary vertical pointing menu iteMenuAdmin">
                    <?if(Session::get('idPerfil') == '1'): ?>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>admin/misdatos">Actualizar mi Perfil</a>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>admin/index">Panel Administrativo</a>
                    <?elseif(Session::get('idPerfil') == '2'):?>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>investigador/misdatos">Actualizar mi Perfil</a>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>investigador/index">Administrar mis videos</a>
                    <?elseif(Session::get('idPerfil') == '3'):?>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>emprendedor/misdatos">Actualizar mi Perfil</a>
                        <a class="item" href="<?echo($config->get('baseUrl'))?>emprendedor/index">Administrar mis videos</a>
                    <?endif;?>
                    <a class="item" href="<?echo($config->get('baseUrl'))?>logout">Salir</a>
                </div>
            </div>
        </div>
    </div>

    <div id="menuP">
        <div class="ui breadcrumb">
            <?foreach($aNavegacion as $sUrl => $sTitulo){?>
                <?if($sUrl){?>
                    <?if((Session::get('idPerfil')) == '1'):?>
                        <a class="section" href="<?echo ($config->get('baseUrl'))?>admin/index" class="section"><?echo $sTitulo?></a>
                    <?elseif(Session::get('idPerfil') == '2'):?>
                        <a class="section" href="<?echo ($config->get('baseUrl'))?>investigador/index" class="section"><?echo $sTitulo?></a>
                    <?elseif(Session::get('idPerfil') == '3'):?>
                        <a class="section" href="<?echo ($config->get('baseUrl'))?>emprendedor/index" class="section"><?echo $sTitulo?></a>
                    <?endif;?>
                    <a class="right angle icon divider"> > </a>
                <?}else{?>
                    <div class="active section"><?echo($sTitulo)?></div>
                <?}?>
            <?}?>
        </div>
    </div>
</div>

<?if($bEstadistica):?>
    <div id="menuEstadistica">
        <p class="titulo">Reportes</p>
        <ul>
            <li>
                <a class="<?echo $activeItem == 'resumen Global' ? 'activo' : '' ?>" href="<?echo ($config->get('baseUrl'))?>admin/estadistica/resumen">Resumen Global</a>
            </li>
            <li>
                <a class="<?echo $activeItem == 'areas del Conocimiento' ? 'activo' : '' ?>"  href="<?echo ($config->get('baseUrl'))?>admin/estadistica/areasConocimiento">Áreas del Conocimiento</a>
            </li>
            <li>
                <a class="<?echo $activeItem == 'investigadores' ? 'activo' : '' ?>" href="<?echo ($config->get('baseUrl'))?>admin/estadistica/investigadores">Investigadores</a>
            </li>
            <li>
                <a class="<?echo $activeItem == 'Urlpersonalizados' ? 'activo' : '' ?>" href="<?echo ($config->get('baseUrl'))?>admin/estadistica/urlpersonalizados">URLs Personalizados</a>
            </li>
            <li>
                <a class="<?echo $activeItem == 'videos' ? 'activo' : '' ?>" href="<?echo ($config->get('baseUrl'))?>admin/estadistica/videos">Videos</a>
            </li>
        </ul>
    </div>

    <div id="barraFiltro">
        <h3>Filtros: </h3>
        <div class="five wide field">

            <div class="ui form">
                <div class="inline fields">
                    <div class="inline fields fechaCalendario">
                        <label>Fecha Inicial:</label>
                        <div class="ui calendar" id="rangestart">
                            <div class="ui input right icon">
                                <i class="calendar icon"></i>
                                <input type="text">
                            </div>
                        </div>
                    </div>
                    <div class="inline fields fechaCalendario">
                        <label>Fecha Final:</label>
                        <div class="ui calendar" id="rangeend">
                            <div class="ui input right icon">
                                <i class="calendar icon"></i>
                                <input type="text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?endif;?>

<script>
    $(document).ready(function(){

        $('#rangestart').calendar({
            type: 'date',
            endCalendar: $('#rangeend'),
            formatter: {
                date: function (date) {
                    if (!date) return '';

                    var dia = date.getDate();
                    var anio = date.getFullYear();
                    var month = new Array();
                    month[0] = "Enero";
                    month[1] = "Febrero";
                    month[2] = "Marzo";
                    month[3] = "Abril";
                    month[4] = "Mayo";
                    month[5] = "Junio";
                    month[6] = "Julio";
                    month[7] = "Agosto";
                    month[8] = "Septiembre";
                    month[9] = "Octubre";
                    month[10] = "Noviembre";
                    month[11] = "Diciembre";

                    var mes = month[date.getMonth()];


                    if (dia < 10) dia = '0' + dia;
                    return dia + ' de ' + mes + ' de ' + anio;
                }
            }
        });
        $('#rangeend').calendar({
            type: 'date',
            startCalendar: $('#rangestart'),
            formatter: {
                date: function (date) {
                    if (!date) return '';

                    var dia = date.getDate();
                    var anio = date.getFullYear();
                    var month = new Array();
                    month[0] = "Enero";
                    month[1] = "Febrero";
                    month[2] = "Marzo";
                    month[3] = "Abril";
                    month[4] = "Mayo";
                    month[5] = "Junio";
                    month[6] = "Julio";
                    month[7] = "Agosto";
                    month[8] = "Septiembre";
                    month[9] = "Octubre";
                    month[10] = "Noviembre";
                    month[11] = "Diciembre";

                    var mes = month[date.getMonth()];


                    if (dia < 10) dia = '0' + dia;
                    return dia + ' de ' + mes + ' de ' + anio;
                }
            }
        });
    });
</script>


