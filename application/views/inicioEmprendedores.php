<div id="menuVideos" class="ui borderless menu">
    <div class="item">
        <p class="txtAzul">Filtrar videos por:</p>
    </div>

    <a href="<?echo($bEmprendimiento == '1' ? $config->get('baseUrl') . 'emprendimiento' : '')?>" class="item">
        <div class="ui mini image">
            <img src="<?echo($config->get('baseUrl'))?>assets/img/clock.png">
        </div>
        <div class="middle aligned content txtAzul bordeAzul">
            Videos más recientes
        </div>
    </a>

    <div id="menuAreas" class="ui menu transparente">
        <div class="ui dropdown item">
            <div class="ui mini image">
                <img src="<?echo($config->get('baseUrl'))?>assets/img/areas.png">
            </div>
            <div class="middle aligned content txtAzul bordeAzul">
                Áreas del Conocimiento
            </div>
            <i class="dropdown icon"></i>
            <div id="dropdownAreas" class="menu">
                <?foreach($areasConocimiento as $area):?>
                    <a href="<?echo($bEmprendimiento == '1' ? $config->get('baseUrl') . 'emprendimiento/videos/area?id=' . $area['id'] : $config->get('baseUrl') . 'videos/area?id=' . $area['id'])?>" class="item"><? echo $area['nombre'];?></a>
                <?endforeach;?>
            </div>
        </div>
    </div>

    <div id="menuEmprendimiento" class="ui right menu transparente">
        <a href="<?echo($config->get('baseUrl'))?>" id="itemEmprendimiento" class="item">
            <div class="ui mini image">
                <img src="<?echo($config->get('baseUrl'))?>assets/img/videoResumenes.png">
            </div>
            <p class="txtAzul">Video-resúmenes de papers</p>
        </a>
    </div>
</div>

<div id="contenido">
    <div class=" txtContPHD">
        <div class="reloj">
            <img src="<?echo($config->get('baseUrl'))?>assets/img/clock2.png">
        </div>
        <div>
            <p>  Estos son los videos más recientes:</p>
        </div>
    </div>

    <div id="contenedorVideos">
        <div id="videosRecientes" class="ui stackable three column grid container left aligned">
            <?foreach($aVideosRecientes as $aVideo){?>
                <div class="column">
                    <div id="cardImg" class="ui card aligned center">
                        <a class="image" href="<?echo($config->get('baseUrl'))?>emprendimiento/video/visualizar?id=<?echo($aVideo['id'])?>">
                            <img class="imagPhd" src="https://img.youtube.com/vi/<?echo($aVideo['imagen'])?>/mqdefault.jpg">
                        </a>
                        <div id="datosVideos" class="content left ">
                            <a class="header" href="<?echo($config->get('baseUrl'))?>emprendimiento/video/visualizar?id=<?echo($aVideo['id'])?>"><?echo($aVideo['titulo'])?></a>
                            <div class="description">
                                <p>Emprendedor: <span class="txtSubcribe"><?echo($aVideo['nombre']) . ' ' . $aVideo['apellido_p'] . ' ' . $aVideo['apellido_m']?></span><br>
                                    Área del conocimiento: <span class="txtSubcribe"</span><?echo ($aVideo['areaConocimiento'])?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?}?>
        </div>
    </div>

</div>

<script>

    $(document).ready(function(){

        var $nPaginaActual = 1;

        $(window).scroll(function(){
            if($(window).scrollTop() + $(window).height() === $(document).height()){

                $.ajax({
                    type: 'GET',
                    url: '<?echo($config->get('baseUrl'))?>ajaxObtenerVideosEmprendedores?p=' + $nPaginaActual,
                    contentType: "application/json",
                    dataType: 'json',
                    success: function(result){

                        Object.keys(result).forEach(function(index){

                            var $content =
                                '<div class="column">' +
                                '<div id="cardImg" class="ui card aligned center">' +
                                '<a class="image" href="<?echo($config->get('baseUrl'))?>video/visualizar?id='+ result[index].id + '">' +
                                '<img class="imagPhd" src="https://img.youtube.com/vi/'+ result[index].imagen + '/mqdefault.jpg">' +
                                '</a>' +
                                '<div id="datosVideos" class="content left ">' +
                                '<a class="header" href="<?echo($config->get('baseUrl'))?>video/visualizar?id='+ result[index].id +'">' + result[index].titulo + '</a>' +
                                '<div class="description">' +
                                '<p>Investigador: <span class="txtSubcribe">'+ result[index].nombre + ' ' + result[index].apellido_p + ' ' + result[index].apellido_m + '</span><br>' +
                                'Área del conocimiento: <span class="txtSubcribe"</span>'+ result[index].areaConocimiento + '</p>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';

                            $('#videosRecientes').append($content);

                        });

                        $nPaginaActual += 1;

                    }
                });
            }
        });


    })

</script>