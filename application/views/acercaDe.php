<p class="ui tituloBarra azulMarino">Acerca de nosotros</p>




<div id="contenedor">
<h3 class="txtAmarillo center aligned">El Equipo</h3>
    <div class="ui grid stackable gridAcercaDe">
        <div class="four wide column">
            <div class="center aligned">
                <div class="ui image small">
                    <img src="<?echo($config->get('baseUrl'))?>assets/img/lucia.png">
                </div>
            </div>
            <h4>Lucía Rodríguez Aceves, Ph.D.</h4>
            <p>Especialista en emprendimiento e innovación. Profesora investigadora, Tecnológico de Monterrey, Campus Guadalajara (S.N.I. nivel candidato).</p>
        </div>
        <div class="four wide column">
            <div class="center aligned">
                <div class="ui image small">
                    <img src="<?echo($config->get('baseUrl'))?>assets/img/gabriel.png">
                </div>
            </div>
            <h4>Gabriel Valerio, Ph.D.</h4>
            <p>Especialista en gestión del conocimiento. Profesor investigador, Tecnológico de Monterrey, Campus Monterrey (S.N.I. nivel 1).</p>
        </div>
        <div class="four wide column">
            <div class="center aligned">
                <div class="ui image small">
                    <img src="<?echo($config->get('baseUrl'))?>assets/img/noemi.png">
                </div>
            </div>
            <h4>Lic. Noemí Sanabria Ibarra</h4>
            <p>Especialista en producción audiovisual, locución y difusión de contenidos digitales. Titafilms.</p>
        </div>
        <div class="four wide column">
            <div class="center aligned">
                <div class="ui image small">
                    <img src="<?echo($config->get('baseUrl'))?>assets/img/ruben.png">
                </div>
            </div>
            <h4>Rubén Valencia, Ph.D.</h4>
            <p>Especialista en User Experience, Líder de Desarrollo de Software. Profesor de Catedra en el Tecnológico de Monterrey, Campus Guadalajara.</p>
        </div>
    </div>
</div>

<div class="fondo gris">
    <div class="ui grid stackable gridAcercaDe">
        <div class="eight wide column">
            <div id="txtAcercaDe">
                <div id="textoJustificado">
                    <h4 class="txtAmarillo">¿Qué es Phd Talks?</h4>
                    <p>Phd Talks es una iniciativa que busca poner la ciencia al alcance de todos, para lo cual utiliza una plataforma web en la que investigadores en distintas áreas de conocimiento pueden difundir sus artículos de investigación publicados en revistas científicas de calidad utilizando un formato de video que es amigable, ágil y en idioma español.</p>

                    <h4 class="txtAmarillo">¿Hacia dónde vamos?</h4>
                    <p>PhD Talks será el observatorio de artículos científicos en formato de video-resumen más completo, preferido y utilizado por nuestros usuarios de habla hispana.</p>
                </div>
            </div>
        </div>
        <div class="eight wide column">
            <div id="videoAcercaDe" class="center aligned">
                <div class="video-responsive">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/D4MSfqj5sVw" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

