<div>
    <h1 class="center aligned azulMarino">Bienvenid@ <?echo(Session::get('nombre'))?></h1>

    <div class="ui container">
        <div id="btnAgregarInvestigador">
            <a class="ui basic button btnAdmin" href="<?echo($config->get('baseUrl'))?>admin/emprendedor/registrar">
                <p>Agregar nuevo Proyecto de Emprendimiento Científico</p>
            </a>
        </div>

        <? if($oNotices = $config->get('flashMessenger')->getMessages('mensajesNotice')):?>
            <div id="mensajeExito" class="ui success message" style="display:  <?php echo($oNotices ? 'block' : '')?>">
                <i class="close icon"></i>
                <ul class="list">
                    <?foreach($oNotices as $sNotice):?>
                        <li><? echo($sNotice)?></li>
                    <? endforeach;?>
                </ul>
            </div>
        <? endif;?>

        <table id="tablaBienvenida" class="ui table striped selectable">
            <thead>
            <tr>
                <th>Nombre del Proyecto</th>
                <th>Nombre del Representante</th>
                <th width="140">Acciones</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<!--mensaje emergente-->
<div id="modalEliminar" class="ui small modal">
    <i class="close icon"></i><!--cierra el icono-->
    <div class="ui header azul barra">
        Eliminar Proyecto de Emprendimiento
    </div>
    <div class="contenido">
        <p>
            Confirme que desea eliminar el Proyecto  <strong class="nombreEmprendedor"></strong> . <br/>
            Esta operación no se puede deshacer, asegúrese de que realmente desea eliminar el Proyecto.
        </p>
    </div>
    <div class="actions">
        <div class="ui button deny btnPhd amarillo">
            <i class="remove icon"></i>
            Cancelar
        </div>
        <button id="btnEliminarEmprendedor" class="ui button btnPhd azul" data-emprendedor="">
            <i class="trash icon"></i>
            Eliminar
        </button>
    </div>
</div>

<script>
    $(document).ready(function(){
        var dt = $('#tablaBienvenida').DataTable({
            /*checar datos*/
            "ajax": "<?echo($config->get('baseUrl'))?>jsonProyectosEmprendimiento",
            bAutoWidth: false,
            "language": {
                url: '<?echo($config->get('baseUrl'))?>assets/json/datatables.es.json'
            },
            "columns": [
                {"data": "nombre"},
                {"data": "nombreEmprendedor"},
                {
                    render: function (data, type, rowData, meta){
                        return '<div class="ui small basic icon buttons">' +
                            '<a class="ui icon basic button" href="<?echo($config->get('baseUrl') . 'admin/emprendedor/editar?emprendedor=')?>' + rowData.idEmprendedor + '&proyecto=' + rowData.id + '" title="Editar"><i class="icon edit black" aria-hidden="true"></i></a>' +
                            '<button class="ui icon basic button btn-eliminar" type="button" title="Eliminar"><i class="icon remove black"></i></button>' +
                            '</div>'
                    }
                }
            ]
        });

        $('#tablaBienvenida tbody').on('click' , 'tr td .btn-eliminar' , function() {
            var tr = $(this).closest('tr');
            var row = dt.row(tr);

            /*mensaje emergente*/
            $('#modalEliminar').modal({
                onShow: function (callback) {
                    var $nombreEmprendedor = $(this).find('.contenido').find('.nombreEmprendedor');
                    $nombreEmprendedor.html(row.data().nombre);
                    var $btnEliminar = $(this).find('.actions').find('#btnEliminarEmprendedor');
                    $btnEliminar.attr('data-emprendedor', row.data().id);
                }
            }).modal('show');
        });

        $("#btnEliminarEmprendedor").click(function(){
            var emprendedor = $(this).data('emprendedor');

            if(emprendedor == undefined) /*si el emprendedor no es definido*/
            {
                alert("No se ha especificado el Proyecto a eliminar");
                return false
            }
            $.post('<?echo ($config->get("baseUrl"))?>emprendedores/ajaxEliminarEmprendedor',{ emprendedor : emprendedor },function (data){

                if(data.success !== true) /*verificar si tuvo exito*/
                {
                    return false;
                }
                window.location.reload();/*actualiza datos en momento real*/

                $('#modalEliminar').modal("hide");

            }, 'json');
        });

    });
</script>