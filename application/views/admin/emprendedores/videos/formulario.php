<?if(Session::get('idPerfil') == 1 && !$bActualizar):?>
    <p class="ui tituloBarra azulMarino"> Agregar video de Proyecto de Emprendimiento Científico</p>
<?else:?>
    <p class="ui tituloBarra azulMarino"><?echo ($bActualizar ? 'Actualizar' : 'Agregar') ?> video de Proyecto de Emprendimiento Científico</p>
<?endif;?>
<div class="fondo gris">

    <div class="ui container">
        <?if($oErrors):?>
            <div class="ui error message">
                <i class="close icon"></i>
                <ul class="list">
                    <?foreach($oErrors->mensajes as $sError):?>
                        <li><? echo($sError)?></li>
                    <? endforeach;?>
                </ul>
            </div>
        <? endif;?>

        <? if($oNotices = $config->get('flashMessenger')->getMessages('mensajesNotice')):?>
            <div class="ui success message">
                <i class="close icon"></i>
                <ul class="list">
                    <?foreach($oNotices as $sNotice):?>
                        <li><? echo($sNotice)?></li>
                    <? endforeach;?>
                </ul>
            </div>
        <? endif;?>
    </div>

    <form id="formdatosvideos" class="ui container form" method="POST" action="">

        <?if(Session::get('idPerfil') == 1 && !$bActualizar):?>
            <div class="field">
                <label>*Emprendedor</label>
                <select class="ui search dropdown" name="form[idEmprendedor]" >
                    <option value="<? echo $aVideo['idEmprendedor'];?>">Emprendedor</option>
                    <?foreach($aEmprendedores as $aEmprendedor){?>
                        <option value="<? echo $aEmprendedor['id'];?>"><?echo $aEmprendedor['nombre']?></option>
                    <?}?>
                </select>
            </div>
        <?endif;?>

        <div class="field">
            <label>*Título del proyecto</label>
            <input id="inputTitulo" type="text" name="form[titulo]" placeholder="Título del proyecto" value="<?echo($aVideo['titulo'])?>">
        </div>

        <div class="field">
            <label>*Área del conocimiento</label>
            <select class="ui search dropdown" id="areasConocimiento" name="form[id_subarea_conocimiento]" >
                <option value="<? echo $aVideo['id_subarea_conocimiento'];?>">Área del conocimiento</option>
                <?foreach($aSubAreasConocimiento as $subArea){?>
                    <option value="<? echo $subArea['id'];?>"><?echo $subArea['area_nombre']?> / <? echo $subArea['subarea_nombre'];?></option>
                <?}?>
            </select>
        </div>

        <div class="field">
            <label>*Descripción qué problema resuelve tu proyecto y cómo lo hace, en español y en un lenguaje amigable</label>
            <textarea id="inputDescripcion" name="form[descripcion]" placeholder="Descripción" rows="4" ><?echo($aVideo['descripcion'])?></textarea>
        </div>

        <div class="field">
            <label for="form[esquema_proteccion]">¿Consideras que tu proyecto podría protegerse bajo algún esquema de propiedad intelectual (patente, modelo de utilidad, diseño industrial, etc.)</label>

        </div>
        <div class="fields">
            <div class="inline fields">
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="form[esquema_proteccion]" tabindex="0" class="hidden" value="1" <?echo($aVideo['esquema_proteccion'] == '1' ? 'checked' : '')?>>
                        <label>Ya está protegido</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="form[esquema_proteccion]" tabindex="0" class="hidden" value="2" <?echo($aVideo['esquema_proteccion'] == '2' ? 'checked' : '')?>>
                        <label>Sí pero aún no está protegido</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="form[esquema_proteccion]" tabindex="0" class="hidden" value="3" <?echo($aVideo['esquema_proteccion'] == '3' ? 'checked' : '')?>>
                        <label>No lo sé, necesito asesoría</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="form[esquema_proteccion]" tabindex="0" class="hidden" value="4" <?echo($aVideo['esquema_proteccion'] == '4' ? 'checked' : '')?>>
                        <label>No lo creo</label>
                    </div>
                </div>
            </div>

        </div>

        <div class="field">
            <label>*Liga Youtube</label>
            <input id="inputLigaYoutube" type="text" name="form[liga_youtube]" placeholder="Liga de Youtube" value="<?echo($aVideo['liga_youtube'])?>">
        </div>

        <div class="field">
            <label>*Liga al resumen ejecutivo en PDF (Drive o Dropbox)</label>
            <input id="inputLigaArticulo" type="text" name="form[liga_resumen]" placeholder="Liga Articulo" value="<?echo($aVideo['liga_resumen'])?>">
        </div>

        <div class="field">
            <label>Palabras Clave:</label>
            <input id="inputPalabraClave" type="text" name="form[palabras_clave]" placeholder="Palabra Clave" value="<?echo($aVideo['palabras_clave'])?>">
        </div>

        <?if(!$bActualizar):?>
            <div class="field">
                <div class="ui checkbox">
                    <input type="checkbox" name="checkbox">
                    <label>Estoy de acuerdo con el <a href="<?echo($config->get('baseUrl'))?>avisoLegal" target="_blank">Aviso Legal</a> y el <a href="<?echo($config->get('baseUrl'))?>avisoPrivacidad" target="_blank">Aviso de Privacidad.</a></label>
                </div>
            </div>
        <?endif;?>

        <?if(Session::get('idPerfil') == 1 && $bActualizar):?>
            <div class="field">
                <div class="ui checkbox">
                    <input type="checkbox" value="1" name="form[bVisible]" <?echo($aVideo['bVisible'] == '0' ? 'checked' : '')?>>
                    <label>Ocultar este video</label>
                </div>
            </div>
        <?endif;?>

        <div class="center">
            <p>Los campos marcados con * son obligatorios.</p>
            <button class="ui button agregarVideo btnAzul" type="submit">Registrar</button>
        </div>

        <div class="ui error message"></div>

    </form>
</div>


<script>
    $(document).ready(function() {
        $(".dropdown").dropdown();

        $('.ui.radio.checkbox')
            .checkbox()
        ;

        $('#formdatosvideos').form({
            on: 'blur',
            fields:{
                titulo:{
                    identifier: 'form[titulo]',
                    rules:[
                        {
                            type   : 'empty',
                            prompt : 'Debe especificar el título'
                        }
                    ]
                },
                subareaConocimiento:{
                    identifier: 'form[id_subarea_conocimiento]',
                    rules:[
                        {
                            type:'empty',
                            prompt : 'Debe especificar la área del conocimiento'
                        }
                    ]
                },
                descripcion:{
                    identifier: 'form[descripcion]',
                    rules:[
                        {
                            type   : 'empty',
                            prompt : 'La descripción del video no es válida'
                        }
                    ]
                },
                ligaYoutube:{
                    identifier: 'form[liga_youtube]',
                    rules:[
                        {
                            type   : 'empty',
                            prompt : 'La liga de YouTube no es válida'
                        },
                        {
                            type   : 'regExp[/watch\]',
                            prompt  : 'La liga de YouTube no es válida'
                        }
                    ]
                },
                ligaArticulo:{
                    identifier: 'form[liga_resumen]',
                    rules:[
                        {
                            type   : 'empty',
                            prompt : 'La liga del resumen no es válida'
                        }
                    ]
                },
                checkbox:{
                    identifier: 'checkbox',
                    rules:[
                        {
                            type   : 'checked',
                            prompt : 'Debe aceptar el aviso de privacidad y aviso legal'
                        }
                    ]
                }
                <?if(Session::get('idPerfil') == 1 && !$bActualizar):?>
                ,
                investigador:{
                    identifier: 'form[id_emprendedor]',
                    rules:[
                        {
                            type:'empty',
                            prompt : 'Debe especificar un emprendedor'
                        }
                    ]
                },
                <?endif;?>
            }
        });
    });
</script>