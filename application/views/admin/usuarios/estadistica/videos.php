<div id="resumenGlobal">
  <canvas id="VideosPublicados_VI" width="300" height="100"></canvas>
  <canvas id="Visitantes_VI" width="300" height="100"></canvas>
  <canvas id="AdquisicionUsuarios_VI" width="300" height="100"></canvas>
  <canvas id="UsuariosActivos_VI" width="300" height="100"></canvas>
  <canvas id="InvestigadoresActivos_VI" width="300" height="100"></canvas>
  <button id="DescargarVP_VI">Descargar PDF</button>
  <button id="DescargarVisi_VI">Descargar PDF</button>
  <button id="DescargarAU_VI">Descargar PDF</button>
  <button id="DescargarUA_VI">Descargar PDF</button>
  <button id="DescargarIA_VI">Descargar PDF</button>
</div>

<script>
Chart.defaults.global.legend.display = false
//Change Background Color to White
var backgroundColor = 'white';
Chart.plugins.register(
  {
    beforeDraw: function(c)
    {
        var ctx = c.chart.ctx;
        ctx.fillStyle = backgroundColor;
        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
    }
  });

  $(document).ready(function()
  {
    new Chart(document.getElementById("VideosPublicados_VI"),
    {
      type: 'line',
      data: {
        labels: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic"],
        datasets: [
          {
            data: [12,1,11,2,10,3,9,4,8,5,7,6],
            borderColor: "#003C6D",
            borderWidth: 5,
            pointBorderWidth: 10,
            fill: false
          }
        ]
      },
      options:
      {
        title:
        {
          display: true,
          fontSize: 32,
          text: 'Histórico de Videos Publicados'
        }
      }
    });

    DescargarVP_VI.addEventListener("click", function ()
    {
      var imgData = document.getElementById('VideosPublicados_VI').toDataURL("image/jpeg", 1.0);
      var pdf = new jsPDF('landscape');
      pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
      pdf.save("download.pdf");
    }, false);


    new Chart(document.getElementById("Visitantes_VI"),
    {
        type: 'bar',
        data:
        {
          labels: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic"],
          datasets: [
            {
              backgroundColor: ["#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712",],
              data: [12,1,11,2,10,3,9,4,8,5,7,6]
            }
          ]
        },
        options:
        {
          legend: { display: false },
          title:
          {
            display: true,
            fontSize: 32,
            text: 'Histórico de Visitantes'
          }
        }
    })

    DescargarVisi_VI.addEventListener("click", function ()
    {
      var imgData = document.getElementById('Visitantes_VI').toDataURL("image/jpeg", 1.0);
      var pdf = new jsPDF('landscape');
      pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
      pdf.save("download.pdf");
    }, false);


    new Chart(document.getElementById("AdquisicionUsuarios_VI"),
    {
    type: 'line',
    data: {
      labels: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic"],
      datasets: [
        {
          data: [12,1,11,2,10,3,9,4,8,5,7,6],
          borderColor: "#003C6D",
          borderWidth: 5,
          pointBorderWidth: 10,
          fill: false
        }
      ]
    },
    options:
    {
      title:
      {
        display: true,
        fontSize: 32,
        text: 'Adquisición de Usuarios'
      }
    }
  });

  DescargarAU_VI.addEventListener("click", function ()
  {
    var imgData = document.getElementById('AdquisicionUsuarios_VI').toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('landscape');
    pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
    pdf.save("download.pdf");
  }, false);

  new Chart(document.getElementById("UsuariosActivos_VI"),
  {
  type: 'pie',
  data:{
    labels: [
        "Activos",
        "Inactivos"
    ],
    datasets: [
        {
            data: [13, 87],
            backgroundColor: [
                "#003C6D",
                "#f3a712"
            ]
        }]
  },
  options:
  {
    title:
    {
      display: true,
      fontSize: 32,
      text: 'Usuarios Activos'
    }
  }
  });

  DescargarUA_VI.addEventListener("click", function ()
  {
    var imgData = document.getElementById('UsuariosActivos_VI').toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('landscape');
    pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
    pdf.save("download.pdf");
  }, false);

  new Chart(document.getElementById("InvestigadoresActivos_VI"),
  {
  type: 'pie',
  data:{
    labels: [
        "Activos",
        "Inactivos"
    ],
    datasets: [
        {
            data: [13, 13],
            backgroundColor: [
                "#003C6D",
                "#f3a712"
            ]
        }]
  },
  options:
  {
    title:
    {
      display: true,
      fontSize: 32,
      text: 'Investigadores Activos'
    }
  }
  });

  DescargarIA_VI.addEventListener("click", function ()
  {
    var imgData = document.getElementById('InvestigadoresActivos_VI').toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('landscape');
    pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
    pdf.save("download.pdf");
  }, false);

  });
  </script>
