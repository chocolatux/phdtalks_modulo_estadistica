<div id="resumenGlobal">
  <canvas id="VideosPublicados_RG" width="300" height="100"></canvas>
  <canvas id="Visitantes_RG" width="300" height="100"></canvas>
  <canvas id="AdquisicionUsuarios_RG" width="300" height="100"></canvas>
  <!--<canvas id="UsuariosActivos_RG" width="300" height="100"></canvas>
  <canvas id="InvestigadoresActivos_RG" width="300" height="100"></canvas>-->
  <button id="DescargarVP_RG">Descargar PDF</button>
  <button id="DescargarVisi_RG">Descargar PDF</button>
  <button id="DescargarAU_RG">Descargar PDF</button>
  <!--<button id="DescargarUA_RG">Descargar PDF</button>
  <button id="DescargarIA_RG">Descargar PDF</button>-->
</div>

<script>

function drawMonthly(){
  $.ajax({
      url: '<?echo($config->get('baseUrl'))?>admin/estadistica/generales',
      type: 'GET',
      success: function(res) {
        console.log(res);
        var Jres = JSON.parse(res);
        console.log(Jres);
        new Chart(document.getElementById("VideosPublicados_RG"),
        {
          type: 'line',
          data: {
            labels: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic"],
            datasets: [
              {
                data: [Jres["1"],Jres["2"],Jres["3"],Jres["4"],Jres["5"],Jres["6"],Jres["7"],Jres["8"],Jres["9"],Jres["10"],Jres["11"],Jres["12"]],
                borderColor: "#003C6D",
                borderWidth: 5,
                pointBorderWidth: 10,
                fill: false
              }
            ]
          },
          options: {
				responsive: true,
				title: {
					display: true,
					text: 'Videos Publicados'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Mes'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: ''
						}
					}]
				}
			}
        });
      }
  });
}

function drawViewsMonthly(){
  $.ajax({
      url: '<?echo($config->get('baseUrl'))?>admin/estadistica/vistas',
      type: 'GET',
      success: function(res) {
        console.log(res);
        var Jres = JSON.parse(res);
        console.log(Jres);
        new Chart(document.getElementById("Visitantes_RG"),
        {
          type: 'bar',
          data:
          {
            labels: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic"],
            datasets: [
              {
                backgroundColor: ["#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712",],
                data: [Jres["1"],Jres["2"],Jres["3"],Jres["4"],Jres["5"],Jres["6"],Jres["7"],Jres["8"],Jres["9"],Jres["10"],Jres["11"],Jres["12"]]
              }
            ]
          },
          options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Chart.js Bar Chart'
					}
				 }
        })
      }
    });
}

function drawFollowers(){
  $.ajax({
      url: '<?echo($config->get('baseUrl'))?>admin/estadistica/seguidores',
      type: 'GET',
      success: function(res) {
        console.log(res);
        var Jres = JSON.parse(res);
        console.log(Jres);
        new Chart(document.getElementById("AdquisicionUsuarios_RG"),
        {
          type: 'line',
          data: {
            labels: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic"],
            datasets: [
              {
             data: [Jres["1"],Jres["2"],Jres["3"],Jres["4"],Jres["5"],Jres["6"],Jres["7"],Jres["8"],Jres["9"],Jres["10"],Jres["11"],Jres["12"]],
             borderColor: "#003C6D",
             borderWidth: 5,
             pointBorderWidth: 10,
             fill: false
           }
         ]
       },
  options:
  {
    title:
    {
      display: true,
      fontSize: 32,
      text: 'Adquisición de Usuarios'
    }
  }
  });
  }
});
}

Chart.defaults.global.legend.display = false
//Change Background Color to White
var backgroundColor = 'white';
Chart.plugins.register(
  {
    beforeDraw: function(c)
    {
        var ctx = c.chart.ctx;
        ctx.fillStyle = backgroundColor;
        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
    }
  });

  $(document).ready(function()
  {
    drawMonthly();
    drawViewsMonthly();
    drawFollowers();


    DescargarVP_RG.addEventListener("click", function ()
    {
      var imgData = document.getElementById('VideosPublicados_RG').toDataURL("image/jpeg", 1.0);
      var pdf = new jsPDF('landscape');
      pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
      pdf.save("download.pdf");
    }, false);

    DescargarVisi_RG.addEventListener("click", function ()
    {
      var imgData = document.getElementById('Visitantes_RG').toDataURL("image/jpeg", 1.0);
      var pdf = new jsPDF('landscape');
      pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
      pdf.save("download.pdf");
    }, false);

  DescargarAU_RG.addEventListener("click", function ()
  {
    var imgData = document.getElementById('AdquisicionUsuarios_RG').toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('landscape');
    pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
    pdf.save("download.pdf");
  }, false);

  /*new Chart(document.getElementById("UsuariosActivos_RG"),
  {
  type: 'pie',
  data:{
    labels: [
        "Activos",
        "Inactivos"
    ],
    datasets: [
        {
            data: [13, 87],
            backgroundColor: [
                "#003C6D",
                "#f3a712"
            ]
        }]
  },
  options:
  {
    title:
    {
      display: true,
      fontSize: 32,
      text: 'Usuarios Activos'
    }
  }
  });

  DescargarUA_RG.addEventListener("click", function ()
  {
    var imgData = document.getElementById('UsuariosActivos_RG').toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('landscape');
    pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
    pdf.save("download.pdf");
  }, false);

  new Chart(document.getElementById("InvestigadoresActivos_RG"),
  {
  type: 'pie',
  data:{
    labels: [
        "Activos",
        "Inactivos"
    ],
    datasets: [
        {
            data: [13, 13],
            backgroundColor: [
                "#003C6D",
                "#f3a712"
            ]
        }]
  },
  options:
  {
    title:
    {
      display: true,
      fontSize: 32,
      text: 'Investigadores Activos'
    }
  }
  });

  DescargarIA_RG.addEventListener("click", function ()
  {
    var imgData = document.getElementById('InvestigadoresActivos_RG').toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('landscape');
    pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
    pdf.save("download.pdf");
  }, false);*/

});
</script>
