<div id="resumenGlobal">
  <canvas id="VideosPublicados_AC" width="300" height="100"></canvas>
  <canvas id="Visitantes_AC" width="300" height="100"></canvas>
  <canvas id="AdquisicionUsuarios_AC" width="300" height="100"></canvas>
  <canvas id="UsuariosActivos_AC" width="300" height="100"></canvas>
  <canvas id="InvestigadoresActivos_AC" width="300" height="100"></canvas>
  <button id="DescargarVP_AC">Descargar PDF</button>
  <button id="DescargarVisi_AC">Descargar PDF</button>
  <button id="DescargarAU_AC">Descargar PDF</button>
  <button id="DescargarUA_AC">Descargar PDF</button>
  <button id="DescargarIA_AC">Descargar PDF</button>
</div>


<script>
Chart.defaults.global.legend.display = false
//Change Background Color to White
var backgroundColor = 'white';
Chart.plugins.register(
  {
    beforeDraw: function(c)
    {
        var ctx = c.chart.ctx;
        ctx.fillStyle = backgroundColor;
        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
    }
  });

  $(document).ready(function()
  {
    new Chart(document.getElementById("VideosPublicados_AC"),
    {
      type: 'line',
      data: {
        labels: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic"],
        datasets: [
          {
            data: [12,11,10,9,8,7,6,5,4,3,2,1],
            borderColor: "#003C6D",
            borderWidth: 5,
            pointBorderWidth: 10,
            fill: false
          }
        ]
      },
      options:
      {
        title:
        {
          display: true,
          fontSize: 32,
          text: 'Histórico de Videos Publicados'
        }
      }
    });

    DescargarVP_AC.addEventListener("click", function ()
    {
      var imgData = document.getElementById('VideosPublicados_AC').toDataURL("image/jpeg", 1.0);
      var pdf = new jsPDF('landscape');
      pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
      pdf.save("download.pdf");
    }, false);


    new Chart(document.getElementById("Visitantes_AC"),
    {
        type: 'bar',
        data:
        {
          labels: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic"],
          datasets: [
            {
              backgroundColor: ["#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712","#f3a712",],
              data: [12,11,10,9,8,7,6,5,4,3,2,1]
            }
          ]
        },
        options:
        {
          legend: { display: false },
          title:
          {
            display: true,
            fontSize: 32,
            text: 'Histórico de Visitantes'
          }
        }
    })

    DescargarVisi_AC.addEventListener("click", function ()
    {
      var imgData = document.getElementById('Visitantes_AC').toDataURL("image/jpeg", 1.0);
      var pdf = new jsPDF('landscape');
      pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
      pdf.save("download.pdf");
    }, false);


    new Chart(document.getElementById("AdquisicionUsuarios_AC"),
    {
    type: 'line',
    data: {
      labels: ["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sept","Oct","Nov","Dic"],
      datasets: [
        {
          data: [12,11,10,9,8,7,6,5,4,3,2,1],
          borderColor: "#003C6D",
          borderWidth: 5,
          pointBorderWidth: 10,
          fill: false
        }
      ]
    },
    options:
    {
      title:
      {
        display: true,
        fontSize: 32,
        text: 'Adquisición de Usuarios'
      }
    }
  });

  DescargarAU_AC.addEventListener("click", function ()
  {
    var imgData = document.getElementById('AdquisicionUsuarios_AC').toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('landscape');
    pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
    pdf.save("download.pdf");
  }, false);

  new Chart(document.getElementById("UsuariosActivos_AC"),
  {
  type: 'pie',
  data:{
    labels: [
        "Activos",
        "Inactivos"
    ],
    datasets: [
        {
            data: [13, 87],
            backgroundColor: [
                "#003C6D",
                "#f3a712"
            ]
        }]
  },
  options:
  {
    title:
    {
      display: true,
      fontSize: 32,
      text: 'Usuarios Activos'
    }
  }
  });

  DescargarUA_AC.addEventListener("click", function ()
  {
    var imgData = document.getElementById('UsuariosActivos_AC').toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('landscape');
    pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
    pdf.save("download.pdf");
  }, false);

  new Chart(document.getElementById("InvestigadoresActivos_AC"),
  {
  type: 'pie',
  data:{
    labels: [
        "Activos",
        "Inactivos"
    ],
    datasets: [
        {
            data: [13, 13],
            backgroundColor: [
                "#003C6D",
                "#f3a712"
            ]
        }]
  },
  options:
  {
    title:
    {
      display: true,
      fontSize: 32,
      text: 'Investigadores Activos'
    }
  }
  });

  DescargarIA_AC.addEventListener("click", function ()
  {
    var imgData = document.getElementById('InvestigadoresActivos_AC').toDataURL("image/jpeg", 1.0);
    var pdf = new jsPDF('landscape');
    pdf.addImage(imgData, 'JPEG', 10, 10, 200, 100);
    pdf.save("download.pdf");
  }, false);

  });
  </script>
