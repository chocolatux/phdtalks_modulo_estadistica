<div>
    <h1 class="center aligned azulMarino">Bienvenid@ <?echo(Session::get('nombre'))?></h1>

    <div class="ui container">
        <div id="btnAgregarUsuario"> <!-- VIV CREAR ESTILO :3-->
            <a class="ui basic button btnAdmin" href="<?echo($config->get('baseUrl'))?>admin/usuarios/registrar">
                <p>Agregar nuevo administrador</p>
            </a>
        </div>

        <? if($oNotices = $config->get('flashMessenger')->getMessages('mensajesNotice')):?>
            <div id="mensajeExito" class="ui success message" style="display:  <?php echo($oNotices ? 'block' : '')?>">
                <i class="close icon"></i>
                <ul class="list">
                    <?foreach($oNotices as $sNotice):?>
                        <li><? echo($sNotice)?></li>
                    <? endforeach;?>
                </ul>
            </div>
        <? endif;?>

        <table id="tablaBienvenida" class="ui table striped selectable">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Correo</th>
                <th width="140">Acciones</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<!--mensaje emergente-->
<div id="modalEliminar" class="ui small modal">
    <i class="close icon"></i><!--cierra el icono-->
    <div class="ui header azul barra">
        Eliminar investigador
    </div>
    <div class="contenido">
        <p>
            Confirme que desea eliminar el investigador <strong class="nombreUsuario"></strong> . <br/> <!-- VIV CREAR CLASE :3-->
            Esta operación no se puede deshacer, asegúrese de que realmente desea eliminar el investigador.
        </p>
    </div>
    <div class="actions">
        <div class="ui button deny btnPhd amarillo">
            <i class="remove icon"></i>
            Cancelar
        </div>
        <button id="btnEliminarUsuario" class="ui button btnPhd azul" data-usuario=""> <!-- VIV CREAR Estilo Btn Eliminar Usuario :3-->
            <i class="trash icon"></i>
            Eliminar Usuario
        </button>
    </div>
</div>

<script>
    $(document).ready(function(){
        var dt = $('#tablaBienvenida').DataTable({
            /*checar datos*/
            "ajax": "<?echo($config->get('baseUrl'))?>jsonUsuarios",
            bAutoWidth: false,
            "language": {
                url: '<?echo($config->get('baseUrl'))?>assets/json/datatables.es.json'
            },
            "columns": [
                {"data": "nombre"},
                {"data": "email"},
                {
                    render: function (data, type, rowData, meta){
                        return '<div class="ui small basic icon buttons">' +
                            '<a class="ui icon basic button" href="<?echo($config->get('baseUrl') . 'admin/usuarios/editar?usuario=')?>' + rowData.ID + '" title="Editar"><i class="icon edit black" aria-hidden="true"></i></a>' +
                            '<button class="ui icon basic button btn-eliminar" type="button" title="Eliminar"><i class="icon remove black"></i></button>' +
                            '</div>'
                    }
                }
            ]
        });

        $('#tablaBienvenida tbody').on('click' , 'tr td .btn-eliminar' , function() {
            var tr = $(this).closest('tr');
            var row = dt.row(tr);

            /*mensaje emergente*/
            $('#modalEliminar').modal({
                onShow: function (callback) {
                    var $nombreUsuario = $(this).find('.contenido').find('.nombreUsuario');
                    /*busca en la clase contenido y busca nombreusuario*/
                    $nombreUsuario.html(row.data().nombre);
                    var $btnEliminar = $(this).find('.actions').find('#btnEliminarUsuario');
                    /*busca la clase acción y busca el id del boton eliminar usuario */
                    $btnEliminar.attr('data-usuario', row.data().ID);
                    /*recuperar el valor del atributo*/
                }
            }).modal('show');
            /*muestra el mensaje*/
        });

        /*acción de eliminar*/
        $("#btnEliminarUsuario").click(function(){
            var usuario = $(this).data('usuario');

            if(usuario == undefined) /*si el usuario no es definido*/
            {
                alert("No se ha especificado el usuario a eliminar");
                return false
            }
            $.post('<?echo ($config->get("baseUrl"))?>usuarios/ajaxEliminarUsuario',{ usuario : usuario },function (data){

                if(data.success !== true) /*verificar si tuvo exito*/
                {
                    return false;
                }
                window.location.reload();/*actualiza datos en momento real*/

                $('#modalEliminar').modal("hide");

            }, 'json');
        });

    });
</script>