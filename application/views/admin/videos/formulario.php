<div id="registroVideo">
    <?if(Session::get('idPerfil') == 1 && !$bActualizar):?>
        <p class="ui tituloBarra azulMarino"> Registro de un nuevo Video</p>
    <?else:?>
        <p class="ui tituloBarra azulMarino"><?echo ($bActualizar ? 'Actualizar' : 'Agregar') ?> video</p>
    <?endif;?>
    <div class="fondo gris">

        <div class="ui container">
            <?if($oErrors):?>
                <div class="ui error message">
                    <i class="close icon"></i>
                    <ul class="list">
                        <?foreach($oErrors->mensajes as $sError):?>
                            <li><? echo($sError)?></li>
                        <? endforeach;?>
                    </ul>
                </div>
            <? endif;?>

            <? if($oNotices = $config->get('flashMessenger')->getMessages('mensajesNotice')):?>
                <div class="ui success message">
                    <i class="close icon"></i>
                    <ul class="list">
                        <?foreach($oNotices as $sNotice):?>
                            <li><? echo($sNotice)?></li>
                        <? endforeach;?>
                    </ul>
                </div>
            <? endif;?>
        </div>



        <form id="formdatosvideos" class="ui form" method="POST" action="">
            <div class="ui container">
                <p class="azulMarino aligned-der">Los campos marcados con * son obligatorios</p>

                <?if(Session::get('idPerfil') == 1 && !$bActualizar):?>
                    <div class="field">
                        <label>Investigador*</label>
                        <select class="ui search dropdown" name="form[idInvestigador]" >
                            <option value="<? echo $aVideo['idInvestigador'];?>">Investigador</option>
                            <?foreach($aInvestigadores as $aInvestigador){?>
                                <option value="<? echo $aInvestigador['id'];?>"><?echo $aInvestigador['nombre']?></option>
                            <?}?>
                        </select>
                    </div>
                <?endif;?>

                <div class="field">
                    <label>Título*</label>
                    <input id="inputTitulo" type="text" name="form[titulo]" placeholder="Título" value="<?echo($aVideo['titulo'])?>">
                </div>

                <div class="field">
                    <label>Coautores</label>
                    <input id="inputLigaArticulo" type="text" name="form[coautores]" placeholder="Coautores" value="<?echo($aVideo['coautores'])?>">
                </div>

                <div class="field">
                    <label>Área del conocimiento*</label>
                    <select class="ui search dropdown" id="areasConocimiento" name="form[id_subarea_conocimiento]" >
                        <option value="<? echo $aVideo['id_subarea_conocimiento'];?>">Área del conocimiento</option>

                        <?foreach($aSubAreasConocimiento as $subArea){?>
                            <option value="<? echo $subArea['id'];?>"><?echo $subArea['area_nombre']?> / <? echo $subArea['subarea_nombre'];?></option>
                        <?}?>
                    </select>
                </div>

                <div class="field">
                    <label>Journal*</label>
                    <input id="inputJournal" type="text" name="form[journal]" placeholder="Journal" value="<?echo($aVideo['journal'])?>">
                </div>

                <div class="field">
                    <label>Descripción*</label>
                    <textarea id="inputDescripcion" name="form[descripcion]" placeholder="Descripción" rows="4" ><?echo($aVideo['descripcion'])?></textarea>
                </div>

                <div class="field">
                    <label>Palabras Clave*</label>
                    <input id="inputPalabraClave" type="text" name="form[palabras_clave]" placeholder="Palabra Clave" value="<?echo($aVideo['palabras_clave'])?>">
                    <p class="txtParentesis">Separe cada Palabra Clave con una coma, por ejemplo: gestión, base tecnológica, humanismo</p>
                </div>

                <div class="field">
                    <label>Liga al Articulo*</label>
                    <input id="inputLigaArticulo" type="text" name="form[liga_articulo]" placeholder="Liga Articulo" value="<?echo($aVideo['liga_articulo'])?>">
                </div>

                <div class="field" id="agregarLigaYoutube" style="display: none">
                    <label>Liga YouTube*</label>
                    <input id="inputLigaYoutube" type="text" name="" placeholder="Liga de Youtube" value="<?echo($aVideo['liga_youtube'])?>">
                </div>

                <?if(!$bActualizar):?>
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" name="checkbox">
                            <label>Estoy de acuerdo con el <a href="<?echo($config->get('baseUrl'))?>avisoLegal" target="_blank">Aviso Legal</a> y el <a href="<?echo($config->get('baseUrl'))?>avisoPrivacidad" target="_blank">Aviso de Privacidad.</a></label>
                        </div>
                    </div>
                <?endif;?>

                <?if(Session::get('idPerfil') == 1 && $bActualizar):?>
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox" value="1" name="form[visible]" <?echo($aVideo['visible'] == '0' ? 'checked' : '')?>>
                            <label>Ocultar este video</label>
                        </div>
                    </div>
                <?endif;?>

                <div class="center" id="btnGuardarVideo" data-btn="">
                    <button class="ui button agregarVideo btnAzul" type="submit">Guardar</button>
                    <p>O en caso de que quieras grabar tu video haz clic en <a class="azulMarino interlineado bold" id="ligaPanel">Panel de Grabación</a></p>
                </div>

                <div class="ui error message"></div>
            </div>

            <div class="center" id="panelGrabacion">
                <p class="bold">¡Ahora solo falta grabar tu video!</p>
                <p>Para continuar haz clic en el botón de <span class="bold">Panel de Grabación:</span></p>

                <button class="ui button btnNaranja" id="btnPanelGrabacion" data-btn="">
                    <img class="camaraImg" src="<?echo($config->get('baseUrl'))?>assets/img/camara.png">
                    <p>Panel de Grabación</p>
                </button>
                <p>O en caso de que hayas solicitado <a class="txtNaranja interlineado bold" href="<?echo($config->get('baseUrl'))?>servicios">grabación profesional</a> y ya te hayamos hecho llegar la liga de tu video</p>
                <a class="azulMarino interlineado bold" id="ligaAYoutube">haz clic aquí para registrarla</a>
            </div>
        </form>

        <div class="ui fullscreen modal">

            <i class="close icon"></i>
            <div class="header center aligned fondoNaranja">
                Panel de Grabación
            </div>

            <div class="ui grid" id="moduloGrabacion">

                <div class="eight wide column">
                    <!--contenedor de la cámara web-->
                    <video id="camaraWeb" autoplay muted></video>

                    <div class="aligned center" id="btnGrabado">
                            <i id="btnIcon2" class="circle icon"></i>
                        <button id="record" class="ui button record bordeNaranja" disabled>
                            <p id="btnIconNombre2">Iniciar grabación</p>
                        </button>
                    </div>

                    <!--contenedor del video grabado-->
                    <video id="videoGrabado" loop controls style="display: none;"></video>

                    <!--play obligatorio-->
                    <button id="play" class="ui button record bordeNaranja" style="display: none">
                        <i id="btnIcon" class="play icon"></i>
                        <p id="btnIconNombre">Reproducir mi video</p>
                    </button>

                </div>
                <div class="eight wide column">
                    <div id="datosVideo">
                        <table id="tablaTutorial" class="ui very basic table">
                            <thead>
                            <tr>
                                <th id="columnaTiempo" th colspan="3" class="center aligned azulMarino">Guión recomendado:</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><i id="fechaTituloArticulo" class="play icon puntero"></i></td>
                                <td class="txtNaranja bold">20 s- </td>
                                <td id="txt1">Título de tú artículo (de preferencia en español).</td>
                            </tr>
                            <tr>
                                <td><i id="fechaObjetivoArticulo" class="play icon puntero"></i></td>
                                <td class="txtNaranja bold">40 s- </td>
                                <td id="txt2">Objetivo de tu articulo.</td>
                            </tr>
                            <tr>
                                <td><i id="fechaResultadoArticulo" class="play icon puntero"></i></td>
                                <td class="txtNaranja bold">1min-</td>
                                <td id="txt3">Cúales fueron los resultados principales.</td>
                            </tr>
                            <tr>
                                <td><i id="flechaAplicarConocimiento" class="play icon puntero"></i></td>
                                <td class="txtNaranja bold">1min-</td>
                                <td id="txt4">De qué forma se puede aplicar este conocimiento/resultados en una empresa o en la sociedad.</td>
                            </tr>
                            </tbody>
                        </table>

                        <div>
                            <div class="item">
                                <p id="txtTiempo">Tiempo trascurrido:</p>
                                <h2>0min. 0s.</h2>
                                <svg>
                                    <circle id="circle" class="circle_animation" r="8em" cy="9.5em"  cx="9.9em"/> <!--NO ME BORRES-->
                                </svg>
                                <div class="circulo"></div>
                            </div>
                            <div id="arbolSeleccion" style="visibility: hidden"><!--class="desactivarBtn"-->
                                <p class="alineacionTxt azulMarino">¿Te gustó como quedó tu video?</p>
                               <div id="lineaTiempoOpciones">
                                   <p class="izq">No</p>
                                   <canvas id="lineasGrabacion"> </canvas>
                                   <p class="der">Si</p>
                               </div>
                                <button class="ui button record bordeRojo" id="volverGrabar"><i class="circle icon"></i><p>Volver a grabar</p></button>
                                <button id="sendToServer" class="ui button record bordeVerde der"><img src="<?echo($config->get('baseUrl'))?>assets/img/download.png"><p>Guardar en PHD Talks</p></button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- include adapter for srcObject shim -->
<script src="<?echo($config->get('baseUrl') . 'YoutubeUploader/web-server/js/main.js')?>"></script>

<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>

<script>
    $(document).ready(function() {
        const inputLigaYoutube = $('#inputLigaYoutube');

        $("#ligaAYoutube").click(function () {
            document.getElementById("btnGuardarVideo").style.display = "block";
            inputLigaYoutube.attr('name', 'form[liga_youtube]');
            document.getElementById('agregarLigaYoutube').style.display = 'inline';
            document.getElementById("panelGrabacion").style.display = "none";
        });

        $("#ligaPanel").click(function () {
            document.getElementById("panelGrabacion").style.display = "block";
            document.getElementById("btnGuardarVideo").style.display = "none";
            document.getElementById('agregarLigaYoutube').style.display = 'none';
        });

        $(".dropdown").dropdown();

        var btnGuardarVideo = $('#btnGuardarVideo');
        var bFlag = 1;

        btnGuardarVideo.click(function () {
            bFlag = 0;
        });

        $('#formdatosvideos').form({
            on: 'blur',
            fields: {
                titulo: {
                    identifier: 'form[titulo]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Debe especificar el título'
                        }
                    ]
                }/*,
                subareaConocimiento: {
                    identifier: 'form[id_subarea_conocimiento]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Debe especificar la área del conocimiento'
                        }
                    ]
                },
                descripcion: {
                    identifier: 'form[descripcion]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'La descripción del video no es válida'
                        }
                    ]
                },
                journal: {
                    identifier: 'form[journal]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Debe especificar el Journal'
                        }
                    ]
                },
                ligaYoutube: {
                    identifier: 'form[liga_youtube]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Ingrese la liga de YouTube'
                        },
                        {
                            type: 'regExp[/watch\]',
                            prompt: 'La liga de YouTube no es válida'
                        }
                    ]
                },
                palabrasClave: {
                    identifier: 'form[palabras_clave]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Las palabras clave son necesarias'
                        }
                    ]
                },
                ligaArticulo: {
                    identifier: 'form[liga_articulo]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'La liga del artículo no es válida'
                        }
                    ]
                },
                checkbox: {
                    identifier: 'checkbox',
                    rules: [
                        {
                            type: 'checked',
                            prompt: 'Debe aceptar el aviso de privacidad y aviso legal'
                        }
                    ]
                }*/
                <?if(Session::get('idPerfil') == 1 && !$bActualizar):?>
                ,
                investigador: {
                    identifier: 'form[investigador]',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'Debe especificar un investigador'
                        }
                    ]
                },
                <?endif;?>
            },
            onSuccess: function (event, fields) {

                var $formVideo = $(this);

                if (bFlag !== 0) {
                    event.preventDefault();

                    $.post('<?echo ($config->get("baseUrl"))?>ajaxValidarSiVideoExiste',{video: $('#inputTitulo').val()},function (data){

                        if(data.success !== true)
                        {
                            alert(data.msg);
                            return;
                        }

                        $('.ui.modal').modal('show');

                        $.post('<?echo($config->get('baseUrl'))?>ajaxAgregarVideoInvestigador', $formVideo.serialize());
                        //$.post('http://phdtalks-bare.local:5000/api/prueba', $formVideo.serialize());

                        $.ajax({
                            url : 'http://127.0.0.1:5000/api/prueba',
                            type: 'POST',
                            data: $formVideo.serialize(),
                            success: function(data) {
                                console.log("sending form data");
                            },
                            error: function(error) {
                                console.log("error");
                                console.log(error);
                            }
                        });


                    }, 'json');

                } else {
                    event.preventDefault();

                    $.post('<?echo($config->get('baseUrl'))?>investigador/videos/agregar', $formVideo.serialize());
                }
            }
        });

        /*reloj*/

        /*reloj prueba*/


        //Se obtiene el boton de pla

        /*
        $("#record").click(function(){
           if(btngraba === 0) {
               s=0,m=0,seg=0;
               iniciarReloj();
           }else if(btngraba === 1){
               stopTimer();
            }else{
               reproducirVideo();
           }
        });
        */

        /*
        $("#volverGrabar").click(function(){
            arbolSeleccion.style.visibility = "hidden";
            btnRecord.style.display = 'inline-block';

        });

        */

        /*canvas*/
        let canvas = document.getElementById("lineasGrabacion");
        var c =canvas.getContext('2d');
        c.fillStyle = '#003C6D';

        //c.fillRect(x,y,width,height);
        c.fillRect(0,100,1,30);
        c.fillRect(0,97,300,4);

        c.fillRect(299,100,1,30);

        c.fillRect(158,0,1,100);

    });

</script>