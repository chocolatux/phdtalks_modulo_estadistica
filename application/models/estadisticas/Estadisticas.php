<?php

class Estadisticas extends ModelBase{

  //AND fechaPub > $FechaMin AND fechaPub < $FechaMax//


  public static function obtenerPublicacionesGenerales($month){
    $oModelo = new Static();
    switch ($month) {
    case 1:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-01-01' AND '2017-01-31'";
        break;
    case 2:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-02-01' AND '2017-02-31'";
        break;
    case 3:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-03-01' AND '2017-03-31'";
        break;
    case 4:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-04-01' AND '2017-04-31'";
        break;
    case 5:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-05-01' AND '2017-05-31'";
        break;
    case 6:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-06-01' AND '2017-06-31'";
        break;
    case 7:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-07-01' AND '2017-07-31'";
        break;
    case 8:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-08-01' AND '2017-08-31'";
        break;
    case 9:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-09-01' AND '2017-09-31'";
        break;
    case 10:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-10-01' AND '2017-10-31'";
        break;
    case 11:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-11-01' AND '2017-11-31'";
        break;
    case 12:
        $sQuery = "SELECT COUNT(videoID) FROM videos_publicados WHERE fechaPub BETWEEN '2017-12-01' AND '2017-12-31'";
        break;
    default:
        echo "error";
   }

    if(!$mVideosPorArea = $oModelo->_db->getAll($sQuery))
    {
        return 0;
    }

    return $mVideosPorArea;
  }

  public static function obtenerVistasGenerales($month){
    $oModelo = new Static();
    switch ($month) {
    case 1:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-01-01' AND '2017-01-31'";
        break;
    case 2:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-02-01' AND '2017-02-31'";
        break;
    case 3:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-03-01' AND '2017-03-31'";
        break;
    case 4:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-04-01' AND '2017-04-31'";
        break;
    case 5:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-05-01' AND '2017-05-31'";
        break;
    case 6:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-06-01' AND '2017-06-31'";
        break;
    case 7:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-07-01' AND '2017-07-31'";
        break;
    case 8:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-08-01' AND '2017-08-31'";
        break;
    case 9:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-09-01' AND '2017-09-31'";
        break;
    case 10:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-10-01' AND '2017-10-31'";
        break;
    case 11:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-11-01' AND '2017-11-31'";
        break;
    case 12:
        $sQuery = "SELECT SUM(vistas) FROM videos_publicados WHERE fechaPub BETWEEN '2017-12-01' AND '2017-12-31'";
        break;
    default:
        echo "error";
   }

    if(!$mVistasPorArea = $oModelo->_db->getAll($sQuery))
    {
        return 0;
    }

    return $mVistasPorArea;
  }

  public static function obtenerSeguidores($month){
    $oModelo = new Static();
    switch ($month) {
    case 1:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-01-01' AND '2017-01-31'";
        break;
    case 2:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-02-01' AND '2017-02-31'";
        break;
    case 3:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-03-01' AND '2017-03-31'";
        break;
    case 4:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-04-01' AND '2017-04-31'";
        break;
    case 5:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-05-01' AND '2017-05-31'";
        break;
    case 6:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-06-01' AND '2017-06-31'";
        break;
    case 7:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-07-01' AND '2017-07-31'";
        break;
    case 8:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-08-01' AND '2017-08-31'";
        break;
    case 9:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-09-01' AND '2017-09-31'";
        break;
    case 10:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-10-01' AND '2017-10-31'";
        break;
    case 11:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-11-01' AND '2017-11-31'";
        break;
    case 12:
        $sQuery = "SELECT seguidores FROM invesigador_seguidores WHERE fechaSnap BETWEEN '2017-12-01' AND '2017-12-31'";
        break;
    default:
        echo "error";
   }

    if(!$mVideosPorArea = $oModelo->_db->getAll($sQuery))
    {
        return false;
    }

    return $mVideosPorArea;
  }



}



 ?>
