<?php

require_once $config->get('middlewareFolder') . 'Autentificar.php';
require_once $config->get('middlewareFolder') . 'Seguridad.php';

class Videos extends ModelBase
{
    protected $sTable = 'videos';
    protected $sPrimaryKey = 'id';

    private static function obtenerReproduccionesVideo($sIdVideoYoutube, $mVideos)
    {
        $oModelo = new static();

        $JSON = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=contentDetails,statistics&id={$sIdVideoYoutube}&key=AIzaSyAQ_qI3Jio--0DUMoQxO_YYtVuQkrZ4ngw");

        $JSON_data = json_decode($JSON);

        foreach($mVideos as $key => $aVideo)
        {
            $mVideos[$key]['reproducciones'] = $JSON_data->items[$key]->statistics->viewCount;
        }

        return $mVideos;
    }

    public static function eliminarVideo($nIdVideo)
    {
        if(!$nIdVideo)
        {
            return array('success' => false, 'msg' => 'No se especificó el video');
        }

        /*Se busca el video*/
        if(!$oVideo = self::findById($nIdVideo))
        {
            return array('success' => false, 'msg' => 'Video no encontrado en la base de datos');
        }

        //Se desactiva el video
        if(!empty($oVideo))
        {
            $oVideo->ON = 0;
            $oVideo->save();
        }

        return array ('success' => true);
    }

    public static function obtenerVideos($nIdInvestigador)
    {
        $oModelo = new static();
        $sIdsVideos = '';

        $sQuery = "SELECT videos.id, videos.visible, videos.coautores, videos.journal, 
                   videos.titulo, videos.descripcion , videos.liga_youtube, videos.liga_articulo, 
                   videos.fecha_captura, areas_conocimiento.nombre as AreaConocimiento, 
                   subareas_conocimiento.nombre as Subarea, videos.palabras_clave,
                   videos.visitas, videos.clics_articulo, videos.id_video_youtube
                   FROM videos
                   INNER JOIN areas_conocimiento ON videos.id_area_conocimiento = areas_conocimiento.id
                   INNER JOIN subareas_conocimiento ON videos.id_subarea_conocimiento = subareas_conocimiento.id
                   WHERE videos.`ON` = 1";

        //Si no tiene los permisos de admin se obtienen sólo los videos de un investigador
        if(!((Session::get('idPerfil')) == '1'))
        {
            $sQuery .= " AND id_investigador = {$nIdInvestigador}";
        }

        if(!$mVideos = $oModelo->_db->getAll($sQuery))
        {
            return false;
        }

        //Se convierten la fecha y la hora a formato humano y se obtiene la reproducción de cada video
        foreach($mVideos as $key => $aVideo)
        {
            $mVideos[$key]['fecha_captura'] = Utils::formatDateTimeToHuman($aVideo['fecha_captura'], $oModelo->_config->get('formatoFechaNombreMes'));

            //Se concatenan los id de los videos para consultar sus estadisticas
            $sIdsVideos .= $mVideos[$key]['id_video_youtube'];

            //Si no es el ùltimo video separar los id por comas
            if($key != count($mVideos)-1)
            {
                $sIdsVideos .= ',';
            }
        }

        $mVideos = $oModelo->obtenerReproduccionesVideo($sIdsVideos, $mVideos);

        return $mVideos;
    }

    /**
     * @param int $nVideoInicial
     * @return mixed
     */
    public static function obtenerVideosRecientes($nVideoInicial = 0)
    {
        $oModelo = new static();

        $sQuery = "SELECT videos.id, videos.coautores, videos.visible, investigadores.nombre, investigadores.apellido_p, investigadores.apellido_m, videos.titulo, videos.descripcion, videos.liga_youtube, videos.liga_articulo,subareas_conocimiento.nombre as subArea, videos.palabras_clave, areas_conocimiento.nombre  as areaConocimiento
                   FROM videos
                   INNER JOIN investigadores
                   ON id_investigador = investigadores.id
                   INNER JOIN areas_conocimiento
                   ON videos.id_area_conocimiento = areas_conocimiento.id
                   INNER JOIN subareas_conocimiento
                   ON videos.id_subarea_conocimiento = subareas_conocimiento.id
				   WHERE videos.`ON` = 1 AND videos.visible = 1
                   ORDER BY videos.fecha_captura DESC
                   LIMIT {$nVideoInicial},9";

        if(!$mVideosRecientes = $oModelo->_db->getAll($sQuery)){
            return false;
        }

        foreach ($mVideosRecientes as &$aVideoReciente)
        {
            parse_str(parse_url($aVideoReciente['liga_youtube'], PHP_URL_QUERY), $aYoutubeParams);
            $aVideoReciente['imagen'] = $aYoutubeParams['v'];
        }

        return $mVideosRecientes;

    }

    public static function obtenerVideosPorArea($nIdArea)
    {
        $oModelo = new static();
        $sQuery ="SELECT videos.id, videos.coautores, investigadores.nombre, investigadores.apellido_p, investigadores.apellido_m, videos.titulo, videos.descripcion, videos.liga_youtube, videos.liga_articulo, subareas_conocimiento.nombre as subArea, videos.palabras_clave, areas_conocimiento.nombre  as areaConocimiento
                  FROM videos
                  INNER JOIN investigadores
                  ON id_investigador = investigadores.id
                  INNER JOIN areas_conocimiento
                  ON videos.id_area_conocimiento = areas_conocimiento.id
                  INNER JOIN subareas_conocimiento
                  ON videos.id_area_conocimiento = subareas_conocimiento.id
                  WHERE videos.`ON` = 1 AND areas_conocimiento.id = {$nIdArea} AND videos.visible = 1
                  ORDER BY videos.fecha_captura DESC";

        if(!$mVideosPorArea = $oModelo->_db->getAll($sQuery))
        {
            return false;
        }

        return $mVideosPorArea;
    }

    public static function obtenerInfoVideo($nIdVideo)
    {
        $oModelo = new static();

        $sQuery = "SELECT videos.id, videos.journal, videos.visible, videos.coautores, investigadores.id as idInvestigador,
                   videos.id_subarea_conocimiento, investigadores.nombre, investigadores.apellido_p, 
                   investigadores.apellido_m, videos.titulo, videos.descripcion, 
                   videos.palabras_clave, videos.liga_youtube, liga_articulo, 
                   areas_conocimiento.nombre as area, subareas_conocimiento.nombre as subArea, videos.palabras_clave,
                   videos.visitas, videos.clics_articulo
                   FROM videos
                   INNER JOIN investigadores ON id_investigador = investigadores.id
                   INNER JOIN subareas_conocimiento ON videos.id_subarea_conocimiento = subareas_conocimiento.id
                   INNER JOIN areas_conocimiento ON videos.id_area_conocimiento = areas_conocimiento.id
                   WHERE videos.id = '$nIdVideo' AND videos.`ON`= 1";

        if(!$aVideo = $oModelo->_db->getOne($sQuery))
        {
            return false;
        }

        //Se obtiene el ID del video de youtube
        parse_str(parse_url($aVideo['liga_youtube'], PHP_URL_QUERY), $aYoutubeParams);

        //Se obtienen las reproducciones totales de youtube
        $aVideo['reproducciones'] = $oModelo->obtenerReproduccionesVideo($aYoutubeParams['v'], $aVideo);

        return $aVideo;
    }

    public static function agregarVideo($aDatosVideo)
    {
        //Si existe el video se edita sino se crea
        if($aDatosVideo['id'])
        {

           if(!$oVideo = Videos::findById($aDatosVideo['id']))
           {
               return false;
           }

            $oVideo->update($aDatosVideo);

        }else{
            //Se guarda el video
            $oVideo = self::create($aDatosVideo);

        }

        return $oVideo->id;

    }

    public static function buscarVideo($sPalabra)
    {
        $oModelo = new static();

        $sQuery = "SELECT videos.id, videos.coautores, videos.titulo, videos.descripcion , videos.liga_youtube, videos.liga_articulo, videos.fecha_captura, areas_conocimiento.nombre as areaConocimiento, subareas_conocimiento.nombre as Subarea, videos.palabras_clave,investigadores.nombre AS nombre, investigadores.apellido_p, investigadores.apellido_m
                   FROM videos
                   INNER JOIN areas_conocimiento
                   ON videos.id_area_conocimiento = areas_conocimiento.id
                   INNER JOIN subareas_conocimiento
                   ON videos.id_subarea_conocimiento = subareas_conocimiento.id
                   INNER JOIN investigadores
                   ON videos.id_investigador = investigadores.id
                   WHERE videos.`ON` = 1 AND videos.visible = 1 AND (videos.titulo LIKE '%$sPalabra%' OR videos.palabras_clave LIKE '%$sPalabra%' OR investigadores.nombre LIKE '%$sPalabra%' OR areas_conocimiento.nombre LIKE '%$sPalabra%' OR investigadores.apellido_p LIKE '%$sPalabra%' OR investigadores.apellido_m LIKE '%$sPalabra%')";

        $mVideos = $oModelo->_db->getAll($sQuery);

        return array('videos' => $mVideos, 'totalVideo' => count($mVideos));
    }
}